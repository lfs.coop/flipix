const svgns = "http://www.w3.org/2000/svg"
const HANDLERS = [
  'onclick',
  'onchange',
  'oninput',
  'onkeypress',
]

function appendContent(element, content) {
  if (typeof content === 'string') element.innerHTML = content
  else if (Array.isArray(content)) {
    for (let item of content)
      if (item !== null)
        element.appendChild(item)
  }
  else if (content !== null) element.appendChild(content)
}

function $el({
  tag = 'div',
  id = null,
  className = null,
  style = null,
  content = null,
  namespace = null,
  ...rest
}) {
  const element = namespace
    ? document.createElementNS(namespace, tag)
    : document.createElement(tag)
  if (id) element.id = id
  if (className) element.className = className
  if (style) {
    for (const [key, val] of Object.entries(style)) {
      element.style[key] = val
    }
  }
  if (content) appendContent(element, content)
  for (const key of Object.keys(rest)) {
    if (HANDLERS.includes(key))
      element[key] = rest[key]
    else
      element.setAttribute(key, rest[key])
  }
  return element
}

function $svg(data) {
  data.namespace = svgns
  const { className } = data
  const el = $el(data)
  if (className) el.setAttribute('class', className)
  return el
}

function $d(content, className = null) {
  return $el({ content, className })
}

function $r(content, { className = null, justify = 'flex-start', align = 'items-center' } = {}) {
  let fullClassName = `flex ${justify} ${align} gap-1`
  if (className !== null)
    fullClassName += ` ${className}`
  return $el({ className: fullClassName, content })
}

function $b({ content, onclick, color = 'gray', disabled = false, className = null, style = null }) {
  let fullClassName = `bg-${color}-200 text-${color}-800 p-2 rounded focus:outline-none`
  if (className !== null)
    fullClassName += ` ${className}`
  const btn = $el({
    tag: 'button',
    className: fullClassName,
    style,
    content,
    onclick
  })
  btn.enable = () => {
    btn.disabled = false
    btn.classList.remove('opacity-50')
    btn.classList.remove('cursor-default')
    btn.classList.add(`hover:bg-${color}-100`)
  }
  btn.disable = () => {
    btn.disabled = true
    btn.classList.add('opacity-50')
    btn.classList.add('cursor-default')
    btn.classList.remove(`hover:bg-${color}-100`)
  }
  btn.activate = () => {
    btn.className = `bg-${color}-800 hover:bg-${color}-700 text-${color}-100 p-2 rounded focus:outline-none`
  }
  btn.deactivate = () => {
    btn.className = `bg-${color}-200 text-${color}-700 p-2 rounded focus:outline-none ${btn.disabled ? 'opacity-50 cursor-default' : `hover:bg-${color}-100`}`
  }
  if (disabled) btn.disable()
  else btn.enable()
  return btn
}

function $Modal({ title, content, onCancel, onConfirm, cancelText = 'Cancel', confirmText = 'Confirm', className = null, width = null, height = null }) {
  let contentStyle = ''
  if (width) contentStyle += `min-width: ${width}px;`
  if (height) contentStyle += `min-height: ${height}px;`
  if (contentStyle) contentStyle = `style="${contentStyle}"`
  const innerHtml = `
    <div class="overlay"></div>
    <div class="content p-4 rounded-md bg-white dark:bg-gray-800 dark:text-white shadow-md text-left"${contentStyle}>
      <h3 class="border-b-2 border-gray-800 dark:border-white mt-2 pb-1 text-lg font-bold">${title}</h3>
      <p class="inner-content my-4"></p>
      <div class="actions flex justify-end items-center gap-2">
        ${onCancel ?
          `<button
            class="btn-cancel bg-gray-200 hover:bg-gray-100 text-gray-800 p-2 rounded focus:outline-none"
          >${cancelText}</button>`
          : ''}
        ${onConfirm ?
          `<button
            class="btn-confirm bg-gray-200 hover:bg-gray-100 text-gray-800 p-2 rounded focus:outline-none"
          >${confirmText}</button>`
          : ''}
      </div>
    </div>
  `
  let fullClassName = 'Modal'
  if (className !== null)
    fullClassName += ` ${className}`
  const modal = $el({
    className: fullClassName,
    content: innerHtml,
  })
  modal.querySelector('.overlay').onclick = onCancel ? onCancel : () => {}
  modal.querySelector('.btn-cancel').onclick = onCancel ? onCancel : () => {}
  modal.querySelector('.btn-confirm').onclick = onConfirm ? onConfirm : () => {}
  appendContent(modal.querySelector('.inner-content'), content)
  return modal
}

function $Checkbox({ label, initialValue, onchange, className = null }) {
  const innerHtml = `
    <div class="text-sm">${label}</div>
    <input
      type="checkbox"
      checked=${initialValue}
    />
    <span class="checkmark"></span>
  `
  let fullClassName = 'Checkbox flex gap-4 items-center'
  if (className !== null)
    fullClassName += ` ${className}`
  const checkbox = $el({
    tag: 'label',
    className: fullClassName,
    content: innerHtml,
  })
  checkbox.querySelector('input').onchange = (e) => onchange(e.target.checked)
  return checkbox
}

function $Slider({
  label,
  initialValue,
  oninput,
  onchange,
  min,
  max,
  step = 1,
  className = null,
  showLimits = true,
  inline = false
}) {
  const innerHtml = `
    ${label ? `<div class="label text-sm${inline ? ' flex-grow' : ' mb-1'}">${label} (${initialValue})</div>` : ''}
    <div class="flex items-center">
      ${showLimits ? `<div class="text-gray-500 text-sm">${min}</div>` : ''}
      <input
        class="Slider w-full mx-2"
        type="range"
        min=${min}
        max=${max}
        step=${step}
        value=${initialValue}
      ></input>
      ${showLimits ? `<div class="text-gray-500 text-sm">${max}</div>` : ''}
    </div>
  `
  const slider = $el({ className, content: innerHtml })
  const labelElement = slider.querySelector('.label')
  if (oninput)
    slider.querySelector('input').oninput = (e) => {
      const value = step === 1 ? parseInt(e.target.value) : parseFloat(e.target.value)
      oninput(value)
      if (labelElement)
        labelElement.innerText = `${label} (${value})`
    }
  if (onchange)
    slider.querySelector('input').onchange = (e) => {
      const value = step === 1 ? parseInt(e.target.value) : parseFloat(e.target.value)
      onchange(value)
      if (labelElement)
        labelElement.innerText = `${label} (${value})`
    }
  return slider
}

function $EditableSlider({
  label,
  initialValue,
  initialMin,
  initialMax,
  oninput,
  onchange,
  step = 1,
  className = null,
  inline = false
}) {
  let slider
  const leftLimit = $el({
    tag: 'input',
    className: 'input input-boxed rounded text-center text-xs py-1',
    type: 'number',
    value: initialMin,
    min: initialMin,
    max: initialMax - step,
  })
  const rightLimit = $el({
    tag: 'input',
    className: 'input input-boxed rounded text-center text-xs py-1',
    type: 'number',
    value: initialMax,
    min: initialMin + step,
    max: initialMax,
  })
  slider = $el({
    className,
    content: [
      $el({
        content: label ? `<div class="label text-sm${inline ? ' flex-grow' : ' mb-1'}">${label} (${initialValue})</div>` : '',
      }),
      $el({
        className: 'flex items-center',
        content: [
          leftLimit,
          $el({
            content: `<input
              class="Slider value-input w-full mx-2"
              type="range"
              min=${initialMin}
              max=${initialMax}
              step=${step}
              value=${initialValue}
            ></input>`
          }),
          rightLimit,
        ]
      })
    ]
  })
  const labelElement = slider.querySelector('.label')
  leftLimit.onchange = (e) => {
    const v = parseInt(e.target.value)
    const currentValue = parseInt(slider.querySelector('input.value-input').value)
    slider.querySelector('input.value-input').setAttribute('min', v)
    if (currentValue < v) {
      oninput(v)
      if (labelElement)
        labelElement.innerText = `${label} (${v})`
    }
  }
  rightLimit.onchange = (e) => {
    const v = parseInt(e.target.value)
    const currentValue = parseInt(slider.querySelector('input.value-input').value)
    slider.querySelector('input.value-input').setAttribute('max', v)
    if (currentValue > v) {
      oninput(v)
      if (labelElement)
        labelElement.innerText = `${label} (${v})`
    }
  }
  if (oninput)
    slider.querySelector('input.value-input').oninput = (e) => {
      const value = parseInt(e.target.value)
      oninput(value)
      if (labelElement)
        labelElement.innerText = `${label} (${value})`
    }
  if (onchange)
    slider.querySelector('input.value-input').onchange = (e) => {
      const value = parseInt(e.target.value)
      onchange(value)
      if (labelElement)
        labelElement.innerText = `${label} (${value})`
    }
  return slider
}

function $Switch({ initialValue, name, onchange, className = null }) {
  const transitionClass = 'transition duration-200 ease-in'
  const innerHtml = `
    <input
      type="checkbox"
      name=${name}
      id=${name}
      class='Switch-checkbox ${transitionClass} absolute block w-4 h-4 rounded-full bg-white border-2 border-gray-300 appearance-none cursor-pointer focus:outline-none'
      checked=${initialValue}
    />
    <label
      html-for=${name}
      class='Switch-label ${transitionClass} block overflow-hidden h-4 rounded-full bg-gray-300 cursor-pointer'
    ></label>
  `
  let fullClassName = 'Switch relative inline-block w-8 select-none'
  if (className !== null)
    fullClassName += ` ${className}`
  const _switch = $el({ className: fullClassName, content: innerHtml })
  if (onchange)
    _switch.querySelector('input').oninput = (e) => onchange(e.target.checked)
  return _switch
}

function $Grid({ label, className, content }) {
  return $el({
    className,
    content: [
      $el({ className: 'text-sm mr-4', content: label }),
      $el({
        className: 'grid gap-1 grid-flow-col auto-cols-min',
        content,
      })
    ]
  })
}

function $Accordion({ header, content, open = true }) {
  const toggleAccordion = () => {
    const on = ! (div.getAttribute('data-toggle') === 'true')
    div.setAttribute('data-toggle', on)
    if (on) {
      headerIcon.setAttribute('d', 'M5 15l7-7 7 7')
      innerContent.classList.remove('hidden')
    } else {
      headerIcon.setAttribute('d', 'M19 9l-7 7-7-7')
      innerContent.classList.add('hidden')
    }
  }

  const headerIcon = $svg({
    tag: 'path',
    d: 'M5 15l7-7 7 7',
    'stroke-linecap': 'round',
    'stroke-linejoin': 'round',
    'stroke-width': 2,
  })
  const headerDiv = $el({
    className: 'Accordion-header flex items-center justify-between',
    content: [
      $el({
        className: 'font-medium',
        content: header,
      }),
      $svg({
        tag: 'svg',
        className: 'text-gray-600 hover:text-gray-400 cursor-pointer',
        width: 24,
        height: 24,
        xmlns: svgns,
        viewBox: '0 0 24 24',
        stroke: 'currentColor',
        fill: 'none',
        content: headerIcon,
        onclick: toggleAccordion,
      }),
    ],
  })
  const innerContent = $el({ className: 'Accordion-content', content })
  const div = document.createElement('div')
  div.setAttribute('data-toggle', open)
  appendContent(div, headerDiv)
  appendContent(div, innerContent)
  return div
}

function $Icon({ width, height, path, stroked = true, color = 'currentColor', strokeWidth = 2, onclick }) {
  /* to use Heroicons: https://heroicons.com/ */
  let element
  if (stroked) {
    element = $svg({
      tag: 'svg',
      width,
      height,
      xmlns: svgns,
      viewBox: '0 0 24 24',
      fill: 'none',
      stroke: color,
      content: $svg({
        tag: 'path',
        'stroke-linecap': 'round',
        'stroke-linejoin': 'round',
        'stroke-width': strokeWidth,
        d: path,
      })
    })
  } else {
    element = $svg({
      tag: 'svg',
      width,
      height,
      xmlns: svgns,
      viewBox: '0 0 20 20',
      fill: color,
      content:
        Array.isArray(path)
        ? path.map((p) => $svg({
          tag: 'path',
          d: p,
        }))
        : $svg({
          tag: 'path',
          d: path,
        })
    })
  }
  if (onclick) {
    element = $el({ content: element })
    element.onclick = onclick
  }
  return element
}

function $Toast({ content, color }) {
  return $el({
    className: `toast bg-${color}-400 rounded-lg p-2 font-bold text-white select-none`,
    content,
  })
}
