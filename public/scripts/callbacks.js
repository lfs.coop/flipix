function setBrushColor(color, layer = null, replay = false, updatePicker = false, updateInput = true) {
  if (layer === null) layer = currentLayer
  brushColors[layer] = color
  if (recording && !replay) {
    pushPenAction({
      type: 'config',
      brushColor: color,
      layer,
    })
  }
  if (updatePicker)
    brushColorPicker.color.hexString = color
  if (updateInput)
    brushColorInput.value = color
}
function setBrushSize(size, layer = null, replay = false, updateInputs = false) {
  if (layer === null) layer = currentLayer
  brushSizes[layer] = size
  brushSizeRatios[layer] = (size - 2) / (40 - 2)
  if (recording && !replay)
    pushPenAction({
      type: 'config',
      brushSize: size,
      layer,
    })
  if (updateInputs) {
    brushSizeSlider.querySelector('input.value-input').value = size
    const l = brushSizeSlider.querySelector('.label')
    l.innerText = l.innerText.replace(/ \([^)]+\)$/, ` (${size})`)
  }
}
async function setBrushType(type, layer = null, replay = false, updateInputs = false) {
  if (layer === null) layer = currentLayer
  brushTypes[layer] = type
  if (recording && !replay)
    pushPenAction({
      type: 'config',
      brushType: type,
      layer,
    })

  if (updateInputs) {
    for (let button of brushTypeButtons) {
      const t = button.getAttribute('data-type')
      if (t === type) {
        button.classList.remove('opacity-50')
      } else {
        button.classList.add('opacity-50')
      }
    }
  }
}

function setSizeRandomness(r) {
  brushConfigs[currentLayer].sizeRandomness = r
}

function setBrushFade(fade, updateCheckbox = false) {
  brushFade = fade
  if (updateCheckbox)
    brushFadeCheckbox.checked = fade
  setCanvasFade()
}
function setBrushFadingSpeed(speed, updateInput = false) {
  brushFadingSpeed = speed
  if (updateInput)
    brushFadingSpeedSlider.value = speed
  setCanvasFade()
}

async function restoreState(frame, layer) {
  // 1. restore brush configuration
  const filteredConfigs = configs.filter((config) => {
    return config.layer === layer && config.frame <= frame
  })
  if (filteredConfigs.length === 0)
    return

  // (sort in desc order along timeline)
  filteredConfigs.sort((a, b) => a.frame < b.frame ? 1 : -1)
  // re-apply most recent config
  const config = filteredConfigs[0]
  if (config.color !== brushColors[config.layer])
    setBrushColor(config.color, layer, true, true)
  if (config.size !== brushSizes[config.layer])
    setBrushSize(config.size, layer, true, true)
  if (config.type !== brushTypes[config.layer])
    await setBrushType(config.type, layer, true, true)

  // // 2. replay actions to get canvas state
  // replayStrokes(strokes.filter((s) => s.start <= frame))
}

function setVideo(v) {
  video = v
  videoPlayer.style.display = v ? 'block' : 'none'
  timeline.style.display = v ? 'flex' : 'none'
  btnRecording.disabled = video === null
  if (video !== null) {
    btnRecording.enable()
    btnLoadSession.enable()
    btnSaveSession.enable()
  }
  toggleVideoHandlers(v !== null)
}

function setVideoMuted(m) {
  videoMuted = m
  if (videoPlayer)
    videoPlayer.muted = m
  if (videoMuted) {
    muteButton.classList.add('opacity-30')
  } else {
    muteButton.classList.remove('opacity-30')
  }
}

function setVideoLoop(l) {
  videoLoop = l
  if (videoLoop) {
    loopButton.classList.remove('opacity-30')
    if (videoPlayer)
      videoPlayer.onended = () => {
        clearCanvas()
        videoPlayer.currentTime = 0
        videoPlayer.play()
      }
  } else {
    loopButton.classList.add('opacity-30')
    if (videoPlayer)
      videoPlayer.onended = pauseVideo
  }
}

function setPlaying(p) {
  playing = p
}

async function setCurrentVideoFrame(f, updateVideoPlayer = true, updateCurrentFrameInput = true, forceRestoreState = false) {
  if (f < 0) {
    f = 0
    timelineCurrentFrameInput.value = f
  }
  if (f >= totalFramesCount) {
    f = totalFramesCount - 1
    timelineCurrentFrameInput.value = f
  }
  const prevVideoFrame = currentVideoFrame
  currentVideoFrame = f
  if (updateVideoPlayer) videoPlayer.currentTime = f / FPS
  if (updateCurrentFrameInput) timelineCurrentFrameInput.value = f
  updateCurrentFrameMarker()
  if (forceRestoreState || currentVideoFrame - prevVideoFrame > 1 || prevVideoFrame > currentVideoFrame)
    await restoreState(f, currentLayer)
  const actions = pullPenActions(f)
  for (const action of actions) {
    if (recording && action.layer === currentLayer) continue
    if (action.type === 'down')
      downHandler(action.event, action.layer)
    else if (action.type === 'move')
      moveHandler(action.event, action.layer)
    else if (action.type === 'up')
      upHandler(action.event, action.layer)
    else if (action.type === 'config') {
      const replay = action.layer !== currentLayer
      const updateInputs = action.layer === currentLayer
      setBrushColor(action.brushColor, action.layer, replay, updateInputs, updateInputs)
      setBrushSize(action.brushSize, action.layer, replay, updateInputs)
      await setBrushType(action.brushType, action.layer, replay, updateInputs)
    }
  }
  if (exporting && currentVideoFrame >= currentExportStartFrame) {
    canvases.forEach((canvas, layer) => {
      const ctx = hiddenCanvas.getContext('2d')
      if (exportedCanvases.includes(canvas)) {
        ctx.clearRect(0, 0, exportWidth * exportVideoScale, exportHeight * exportVideoScale)
        ctx.drawImage(
          canvas,
          exportLeft, exportTop, exportWidth, exportHeight,  // source rect with content to crop
          0, 0, exportWidth * exportVideoScale, exportHeight * exportVideoScale // newCanvas
        )
        exportedFrames[layer].push(
          hiddenCanvas.toDataURL('image/png').replace(/data:image\/png;base64,/, '')
        )
      }
    })
    // if (exportBeautify) {
    //   ctx.drawImage(
    //     videoPlayer,
    //     0, 0, exportVideoWidth, exportVideoHeight,  // source rect with content to crop
    //     0, 0, exportWidth * exportVideoScale, exportHeight * exportVideoScale // newCanvas
    //   )
    //   // canvases.forEach((canvas) => {
    //   //   if (exportedCanvases.includes(canvas)) {
    //   //     ctx.drawImage(
    //   //       canvas,
    //   //       exportLeft, exportTop, exportWidth, exportHeight,  // source rect with content to crop
    //   //       0, 0, exportWidth * exportVideoScale, exportHeight * exportVideoScale // newCanvas
    //   //     )
    //   //   }
    //   // })
    //   exportedFrames[0].push(
    //     hiddenCanvas.toDataURL('image/png').replace(/data:image\/png;base64,/, '')
    //   )
    // } else {
    //   canvases.forEach((canvas, layer) => {
    //     if (exportedCanvases.includes(canvas)) {
    //       ctx.drawImage(
    //         canvas,
    //         exportLeft, exportTop, exportWidth, exportHeight,  // source rect with content to crop
    //         0, 0, exportWidth * exportVideoScale, exportHeight * exportVideoScale // newCanvas
    //       )
    //       exportedFrames[layer].push(
    //         hiddenCanvas.toDataURL('image/png').replace(/data:image\/png;base64,/, '')
    //       )
    //     }
    //   })
    // }
    if (currentVideoFrame >= currentExportEndFrame || videoPlayer.ended)
      endExport()
  }
}

async function setCurrentLayer(layer) {
  if (currentLayer !== null)
    document.getElementById(`layer-picker-${currentLayer}`).classList.remove('selected')
  currentLayer = layer
  document.getElementById(`layer-picker-${layer}`).classList.add('selected')
  const removedLayersBefore = removedLayers.filter((l) => l < currentLayer)
  timelineCurrentLayer.style.bottom = `${2 + 10 * (currentLayer - removedLayersBefore.length)}px`
  await restoreState(currentVideoFrame, currentLayer)
}

async function addLayer(autosetLayer = true) {
  const nLayers = layerPickers.childNodes.length
  if (nLayers === 10) return

  const newLayerId = lastLayer++

  // update dom
  if (createLayerPicker) {
    createLayerPicker.id = `layer-picker-${newLayerId}`
    createLayerPicker.classList.remove('new')
    createLayerPicker.innerText = ''
    createLayerPicker.onclick = async () => await setCurrentLayer(newLayerId)
  }
  appendContent(
    layerRemoveHandles,
    $el({
      id: `layer-remove-handle-${newLayerId}`,
      content: $Icon({
        width: 8,
        height: 8,
        path: 'M6 18L18 6M6 6l12 12',
        strokeWidth: 3,
      }),
      className: 'layer-remove-handle',
      onclick: () => {
        layerToRemove = newLayerId
        toggleRemoveLayerConfirm(true)
      }
    })
  )
  appendContent(
    layerPickers,
    createLayerPicker = $el({
      className: 'layer-picker new',
      content: '+',
      onclick: addLayer,
    })
  )

  // create and init new canvas
  const c = createCanvas(newLayerId)
  if (brushFade) {
    setCanvasFade(c)
  }
  addCanvasHandlers(c)
  appendContent(mainArea, c)
  canvases.push(c)

  // register initial configuration
  brushColors[newLayerId] = initialBrushColor
  brushSizes[newLayerId] = initialBrushSize
  brushTypes[newLayerId] = initialBrushType
  pushPenAction({
    frame: 0,
    type: 'config',
    brushColor: brushColors[currentLayer],
    brushSize: brushSizes[currentLayer],
    brushType: brushTypes[currentLayer],
    layer: newLayerId,
  })

  // jump to new layer
  if (autosetLayer)
    await setCurrentLayer(newLayerId)
}

async function removeLayer(layer) {
  removedLayers.push(layer)

  // update dom
  const layerPicker = document.getElementById(`layer-picker-${layer}`)
  const previousLayerIndex = parseInt(layerPicker.previousSibling.id.replace(/layer-picker-/, ''))
  layerPicker.remove()
  document.getElementById(`layer-remove-handle-${layer}`).remove()

  removeCanvas(layer)
  clearPenActionsOnLayer(layer)

  // jump to previous layer
  currentLayer = null
  await setCurrentLayer(previousLayerIndex)
}

async function setRelativeCurrentVideoFrame(r, updateVideoPlayer = true, updateCurrentFrameInput = true) {
  const { width: w } = timelineBackground.getBoundingClientRect()
  const f = timelineStartFrame + Math.round(r * (timelineEndFrame - timelineStartFrame) / w)
  setCurrentVideoFrame(f, updateVideoPlayer, updateCurrentFrameInput)
}

function setTimelineStartFrame(f, updateTimelineStartFrameInput = true) {
  if (f < 0) f = 0
  if (f >= totalFramesCount) f = totalFramesCount - 1
  timelineStartFrame = f
  if (updateTimelineStartFrameInput) timelineStartFrameInput.value = f
  updateTimeline()
}

function setTimelineEndFrame(f, updateTimelineEndFrameInput = true) {
  if (f < 0) f = 0
  if (f >= totalFramesCount) f = totalFramesCount - 1
  timelineEndFrame = f
  if (updateTimelineEndFrameInput) timelineEndFrameInput.value = f
  updateTimeline()
}

function setVideoPadding(padding) {
  if (!videoPlayer) return
  videoPadding = padding
  videoPlayer.style.padding = `${padding}px`
}

function setPlaybackSpeed(s) {
  playbackSpeed = s
  if (!videoPlayer) return
  videoPlayer.playbackRate = playbackSpeed
}

function setRecording(r) {
  recording = r
  recordingOverlay.style.display = r ? 'block' : 'none'
  if (recording) {
    setTesting(false)
    btnRecording.activate()
    if (autoplayOnRecord && videoPlayer) {
      videoPlayer.play()
      replaceConfigActionIfNeeded(currentVideoFrame, currentLayer)
    }
  } else {
    btnRecording.disabled = video === null
    btnRecording.deactivate()
    if (videoPlayer) {
      videoPlayer.pause()
      clearCanvas()
    }
  }
}

function setTesting(t) {
  testing = t
  testingOverlay.style.display = t ? 'block' : 'none'
  testingCanvasControls.style.display = t ? 'grid' : 'none'
  videoContainer.style.display = t ? 'none' : 'block'
  timeline.style.display = (t || video === null) ? 'none' : 'flex'
  if (testing) {
    setRecording(false)
    btnTesting.activate()
    setCanvasSize('full')
  } else {
    btnTesting.deactivate()
    if (video !== null)
      setCanvasSize('video')
    clearCanvas()
  }
}

function zipFramesData(layersData) {
  return new Promise((resolve) => {
    let frameFilename
    const zip = new JSZip()
    let layerIndex = 0
    for (const framesData of Object.values(layersData)) {
      const framesZip = zip.folder(`layer-${layerIndex}`)
      for (const frameIndex in framesData) {
        frameFilename = (parseInt(currentExportStartFrame) + parseInt(frameIndex)).toString().padStart(4, '0')
        framesZip.file(`${frameFilename}.png`, framesData[frameIndex], { base64: true })
      }
      layerIndex++
    }
    zip.generateAsync({ type: 'blob' })
      .then(function(content) {
        const link = document.createElement('a')
        link.download = 'frames.zip'
        link.href = window.URL.createObjectURL(content)
        link.click()
        link.remove()
        resolve()
      })
  })
}

function setExporting(e) {
  exporting = e
  exportingOverlay.style.display = e ? 'grid' : 'none'
  if (exporting) {
    exportedCanvases = canvases.filter((_, layer) => strokes.some((s) => s.layer === layer))
    exportedFrames = canvases.reduce((acc, canvas, idx) => {
      return exportedCanvases.includes(canvas) ? { ...acc, [idx]: [] } : acc
    }, {})
    if (exportMode === 'all') {
      currentExportStartFrame = 0
      currentExportEndFrame = totalFramesCount - 1
    } else if (exportMode === 'zoomed') {
      currentExportStartFrame = timelineStartFrame
      currentExportEndFrame = timelineEndFrame
    } else if (exportMode === 'custom') {
      currentExportStartFrame = exportStartFrame
      currentExportEndFrame = exportEndFrame
    }
    exportLeft = exportCropMargins ? videoPadding : 0;
    exportTop = exportCropMargins ? videoPadding : 0;
    exportWidth = canvases[0].width - (exportCropMargins ? videoPadding * 2 : 0)
    exportHeight = canvases[0].height - (exportCropMargins ? videoPadding * 2 : 0)
    hiddenCanvas.width = exportWidth * exportVideoScale
    hiddenCanvas.height = exportHeight * exportVideoScale
    const bbox = videoPlayer.getBoundingClientRect()
    exportVideoWidth = bbox.width
    exportVideoHeight = bbox.height
    exportMessage.innerText = 'Step 1/2: Extracting frames'
    if (videoPlayer) {
      videoPlayer.currentTime = 0
      videoPlayer.play()
    }
  }
}

function endExport() {
  exportMessage.innerText = 'Step 2/2: Creating zip archive'
  videoPlayer.currentTime = 0
  videoPlayer.pause()
  zipFramesData(exportedFrames).then(() => {
    exportMessage.innerText = 'Done!'
    setTimeout(() => {
      setExporting(false)
    }, 300)
  })
}

function setTimelineZoom(zoom) {
  timelineZoom = zoom
}

function setDarkTheme(on) {
  darkThemeIsOn = on
  localStorage.theme = darkThemeIsOn ? 'dark' : 'light'
  if (darkThemeIsOn)
    document.documentElement.classList.add('dark')
  else
    document.documentElement.classList.remove('dark')
}
