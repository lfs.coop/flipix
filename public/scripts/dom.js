function createVideoFileInput() {
  const input = $el({
    tag: 'input',
    type: 'file',
    hidden: true,
    accept: '.mp4,.mov',
  })
  input.onchange = async () => {
    if (video === null) {
      await changeVideo(input)
    }
    else {
      currentVideoFileInput = input
      toggleChangeVideoConfirm(true)
    }
  }
  return input
}

function createVideoPlayer() {
  videoPlayer = $el({
    id: 'background-video',
    tag: 'video',
    className: 'w-full h-full',
    disablePictureInPicture: true,
    content: `
      <source type="video/mp4"></source>
      <source type="video/mov"></source>
      Your browser does not support this video format.
    `,
  })
  videoPlayer.onplay = startCheckFrameInterval
  videoPlayer.onpause = stopCheckFrameInterval

  const videoPlaceholderFileInput = createVideoFileInput()
  const VideoPlaceholder = $el({
    className: 'w-full h-full grid place-items-center select-none',
    content: [
      videoPlaceholderFileInput,
      $el({
        tag: 'button',
        id: 'video-placeholder-file-input-button',
        className: 'text-gray-500 hover:text-gray-300 focus:outline-none p-4 border-2 border-gray-500 hover:border-gray-300 border-dashed rounded flex flex-col items-center',
        content: [
          $svg({
            tag: 'svg',
            width: 32,
            height: 32,
            xmlns: svgns,
            viewBox: '0 0 24 24',
            fill: 'none',
            stroke: 'currentColor',
            content: $svg({
              tag: 'path',
              'stroke-linecap': 'round',
              'stroke-linejoin': 'round',
              'stroke-width': 2,
              d: 'M4 16v1a3 3 0 003 3h10a3 3 0 003-3v-1m-4-8l-4-4m0 0L8 8m4-4v12',
            })
          }),
          $el({ tag: 'span', content: 'Choose a video file (.mp4, .mov)' })
        ],
        onclick: () => videoPlaceholderFileInput.click(),
      }),
    ]
  })

  return $el({
    id: 'video-player',
    content: [
      videoPlayer,
      VideoPlaceholder,
    ]
  })
}

function createTimeline() {
  const ChangeVideoInput = createVideoFileInput()
  const ChangeVideoButton = $b({
    content: $el({
      content: 'Change video...',
      className: 'text-xs',
      style: { height: '20px', 'line-height': '20px' }
    }),
    onclick: () => ChangeVideoInput.click(),
    className: 'mx-1',
    style: { 'padding-top': '0', 'padding-bottom': '0' }
  })
  playPauseButtonIcon = $svg({
    tag: 'path',
    'fill-rule': 'evenodd',
    'clip-rule': 'evenodd',
    d: 'M10 18a8 8 0 100-16 8 8 0 000 16zM9.555 7.168A1 1 0 008 8v4a1 1 0 001.555.832l3-2a1 1 0 000-1.664l-3-2z',
  })
  const PlayPauseButton = $el({
    tag: 'button',
    className: 'focus:outline-none text-black hover:text-gray-500',
    content: $svg({
      tag: 'svg',
      width: 20,
      height: 20,
      xmlns: svgns,
      viewBox: '0 0 20 20',
      fill: 'currentColor',
      content: playPauseButtonIcon,
    }),
    onclick: () => {
      if (playing) pauseVideo()
      else playVideo()
    }
  })

  const RewindButton = $el({
    tag: 'button',
    className: 'focus:outline-none text-black hover:text-gray-500',
    content: $svg({
      tag: 'svg',
      width: 20,
      height: 20,
      xmlns: svgns,
      viewBox: '0 0 20 20',
      fill: 'currentColor',
      content: $svg({
        tag: 'path',
        'fill-rule': 'evenodd',
        'clip-rule': 'evenodd',
        d: 'M8.445 14.832A1 1 0 0010 14v-2.798l5.445 3.63A1 1 0 0017 14V6a1 1 0 00-1.555-.832L10 8.798V6a1 1 0 00-1.555-.832l-6 4a1 1 0 000 1.664l6 4z',
      }),
    }),
    onclick: () => setCurrentVideoFrame(0)
  })

  const FastForwardButton = $el({
    tag: 'button',
    className: 'focus:outline-none text-black hover:text-gray-500',
    content: $svg({
      tag: 'svg',
      width: 20,
      height: 20,
      xmlns: svgns,
      viewBox: '0 0 20 20',
      fill: 'currentColor',
      content: $svg({
        tag: 'path',
        'fill-rule': 'evenodd',
        'clip-rule': 'evenodd',
        d: 'M4.555 5.168A1 1 0 003 6v8a1 1 0 001.555.832L10 11.202V14a1 1 0 001.555.832l6-4a1 1 0 000-1.664l-6-4A1 1 0 0010 6v2.798l-5.445-3.63z',
      }),
    }),
    onclick: () => setCurrentVideoFrame(totalFramesCount - 1)
  })
  
  timelineTotalFramesCountLabel = $el({
    className: 'text-sm',
    content: '?',
  })
  timelineCurrentFrameInput = $el({
    tag: 'input',
    className: 'input rounded text-center mr-1 text-sm p-1',
    type: 'number',
    min: 0,
    onchange: (e) => setCurrentVideoFrame(parseInt(e.target.value), true, false),
  })

  muteButton = $el({
    tag: 'button',
    className: 'focus:outline-none text-black hover:text-gray-500',
    content: $svg({
      tag: 'svg',
      width: 20,
      height: 20,
      xmlns: svgns,
      viewBox: '0 0 20 20',
      fill: 'currentColor',
      content: $svg({
        tag: 'path',
        'fill-rule': 'evenodd',
        'clip-rule': 'evenodd',
        d: 'M9.383 3.076A1 1 0 0110 4v12a1 1 0 01-1.707.707L4.586 13H2a1 1 0 01-1-1V8a1 1 0 011-1h2.586l3.707-3.707a1 1 0 011.09-.217zM14.657 2.929a1 1 0 011.414 0A9.972 9.972 0 0119 10a9.972 9.972 0 01-2.929 7.071 1 1 0 01-1.414-1.414A7.971 7.971 0 0017 10c0-2.21-.894-4.208-2.343-5.657a1 1 0 010-1.414zm-2.829 2.828a1 1 0 011.415 0A5.983 5.983 0 0115 10a5.984 5.984 0 01-1.757 4.243 1 1 0 01-1.415-1.415A3.984 3.984 0 0013 10a3.983 3.983 0 00-1.172-2.828 1 1 0 010-1.415z',
      }),
    }),
    onclick: () => setVideoMuted(!videoMuted)
  })

  loopButton = $el({
    tag: 'button',
    className: 'focus:outline-none text-black hover:text-gray-500',
    content: $svg({
      tag: 'svg',
      width: 20,
      height: 20,
      xmlns: svgns,
      viewBox: '0 0 20 20',
      fill: 'currentColor',
      content: $svg({
        tag: 'path',
        'fill-rule': 'evenodd',
        'clip-rule': 'evenodd',
        d: 'M4 2a1 1 0 011 1v2.101a7.002 7.002 0 0111.601 2.566 1 1 0 11-1.885.666A5.002 5.002 0 005.999 7H9a1 1 0 010 2H4a1 1 0 01-1-1V3a1 1 0 011-1zm.008 9.057a1 1 0 011.276.61A5.002 5.002 0 0014.001 13H11a1 1 0 110-2h5a1 1 0 011 1v5a1 1 0 11-2 0v-2.101a7.002 7.002 0 01-11.601-2.566 1 1 0 01.61-1.276z',
      }),
    }),
    onclick: () => setVideoLoop(!videoLoop)
  })

  const Controls = $el({
    className: 'w-full flex gap-1 items-center justify-between',
    content: [
      $el({
        className: 'flex',
        id: 'timeline-buttons',
        content: [
          ChangeVideoInput,
          ChangeVideoButton,
          PlayPauseButton,
          RewindButton,
          FastForwardButton,
          muteButton,
          loopButton,
          $el({
            className: 'flex items-center mx-4',
            content: [
              timelineCurrentFrameInput,
              timelineTotalFramesCountLabel,
            ],
          }),
          $Checkbox({
            label: 'Autoplay on record',
            className: 'text-sm',
            initialValue: autoplayOnRecord,
            onchange: (p) => autoplayOnRecord = p,
          }),
        ]
      }),
      $el({
        className: 'flex gap-1 items-center',
        content: [
          timelineStartFrameInput = $el({
            tag: 'input',
            className: 'input rounded text-center mr-1 text-sm p-1',
            style: { 'max-width': '60px' },
            type: 'number',
            onchange: (e) => setTimelineStartFrame(parseInt(e.target.value), false),
          }),
          timelineEndFrameInput = $el({
            tag: 'input',
            className: 'input rounded text-center mr-1 text-sm p-1',
            style: { 'max-width': '60px' },
            type: 'number',
            onchange: (e) => setTimelineEndFrame(parseInt(e.target.value), false),
          }),
        ],
      }),
      // $el({
      //   className: 'flex gap-1 items-center',
      //   content: [
      //     $svg({
      //       tag: 'svg',
      //       width: 16,
      //       height: 16,
      //       xmlns: svgns,
      //       viewBox: '0 0 24 24',
      //       fill: 'none',
      //       stroke: 'currentColor',
      //       content: $svg({
      //         tag: 'path',
      //         'stroke-linecap': 'round',
      //         'stroke-linejoin': 'round',
      //         'stroke-width': 2,
      //         d: 'M21 21l-6-6m2-5a7 7 0 11-14 0 7 7 0 0114 0zM13 10H7',
      //       })
      //     }),
      //     $Slider({
      //       initialValue: timelineZoom,
      //       min: 1,
      //       max: 10,
      //       step: 0.5,
      //       oninput: setTimelineZoom,
      //       onchange: setTimelineZoom,
      //       showLimits: false,
      //     }),
      //     $svg({
      //       tag: 'svg',
      //       width: 16,
      //       height: 16,
      //       xmlns: svgns,
      //       viewBox: '0 0 24 24',
      //       fill: 'none',
      //       stroke: 'currentColor',
      //       content: $svg({
      //         tag: 'path',
      //         'stroke-linecap': 'round',
      //         'stroke-linejoin': 'round',
      //         'stroke-width': 2,
      //         d: 'M21 21l-6-6m2-5a7 7 0 11-14 0 7 7 0 0114 0zM10 7v3m0 0v3m0-3h3m-3 0H7',
      //       })
      //     }),
      //   ]
      // })
    ],
  })

  layerRemoveHandles = $el({
    id: 'layer-remove-handles',
    content: [
      $el({
        content: '',
        id: 'layer-remove-handle-0',
        className: 'layer-remove-handle disabled',
        onclick: async () => await removeLayer(0)
      }),
    ]
  })
  layerPickers = $el({
    id: 'layer-pickers',
    content: [
      $el({
        id: 'layer-picker-0',
        className: 'layer-picker',
        onclick: async () => await setCurrentLayer(0)
      }),
      createLayerPicker = $el({
        className: 'layer-picker new',
        content: '+',
        onclick: addLayer
      }),
    ]
  })

  const TimelineZoomIndicatorWrapper = $el({
    style: { 'max-height': '24px' },
    id: 'timeline-zoom-wrapper',
    content: [
      timelineZoomIndicator = $el({
        id: 'timeline-zoom-indicator',
      }),
      timelineZoomIndicatorLeftHandle = $el({
        tag: 'input',
        type: 'range',
        min: 0,
        max: 100,
        step: 1,
        value: 0,
        oninput: updateTimelineZoomIndicatorLeftHandle,
      }),
      timelineZoomIndicatorRightHandle = $el({
        tag: 'input',
        type: 'range',
        min: 0,
        max: 100,
        step: 1,
        value: 0,
        oninput: updateTimelineZoomIndicatorRightHandle,
      }),
      timelineZoomIndicatorCurrentFrame = $el({
        id: 'timeline-zoom-indicator-current-frame',
        className: 'absolute',
      }),
      timelineZoomIndicatorDragger = $el({
        id: 'timeline-zoom-indicator-dragger',
        className: 'absolute',
      })
    ],
  })

  timelineReferenceFramesWrapper = $el({
    id: 'timeline-reference-frames-wrapper',
  })

  timelineCurrentFrame = $el({
    id: 'timeline-current-frame',
  })
  timelineCurrentLayer = $el({
    id: 'timeline-current-layer',
  })
  timelineBackground = $el({
    className: 'relative bg-gray-400 w-full h-full flex-1 overflow-x-hidden',
    content: [ timelineCurrentFrame, timelineCurrentLayer, timelineReferenceFramesWrapper ],
    onclick: (e) => {
      if (selectedStroke !== null) {
        selectedStroke.domElement.classList.remove('selected')
        selectedStroke = null
      } else if (selectedConfig !== null) {
        selectedConfig.domElement.classList.remove('selected')
        selectedConfig = null
      } else {
        setRelativeCurrentVideoFrame(e.offsetX)
      }
    }
  })

  return $el({
    id: 'timeline',
    className: 'p-2 bg-gray-200 dark:bg-gray-700 gap-2 flex flex-col items-center',
    content: [
      Controls,
      $el({
        className: 'w-full h-full flex',
        content: [
          layerRemoveHandles,
          layerPickers,
          $el({
            className: 'flex flex-col flex-1 ml-1',
            content: [
              TimelineZoomIndicatorWrapper,
              timelineBackground,
            ]
          })
        ]
      })
    ]
  })
}

function createCanvas(layer = 0) {
  let width, height
  if (canvasMode === 'video') {
    const bbox = videoPlayer.getBoundingClientRect()
    width = bbox.width
    height = bbox.height
  } else {
    width = window.innerWidth - Math.max(window.innerWidth * 0.25, 150)
    height = window.innerHeight - 80
  }

  if (layer >= 0)
    id = `draw-canvas-${layer}`
  return $el({
    tag: 'canvas',
    id,
    className: 'draw-canvas',
    width,
    height,
  })
}

function createExportModal() {
  const contentExportZoomed = $r([
    $el({ content: 'from' }),
    exportInputZoomedStart = $el({
      tag: 'input',
      className: 'input input-boxed rounded text-center cursor-default',
      value: timelineStartFrame,
      readonly: true
    }),
    $el({ content: 'to' }),
    exportInputZoomedEnd = $el({
      tag: 'input',
      className: 'input input-boxed rounded text-center cursor-default',
      value: timelineEndFrame,
      readonly: true
    }),
  ], { className: 'my-1' })

  const contentExportCustom = $r([
    $el({ content: 'from' }),
    exportInputCustomStart = $el({
      tag: 'input',
      className: 'input input-boxed rounded text-center flex-grow',
      value: timelineStartFrame,
      type: 'number',
      min: 0,
      max: totalFramesCount - 2,
      onchange: (e) => {
        exportStartFrame = parseInt(e.target.value)
        e.stopPropagation()
      },
      oninput: (e) => {
        exportStartFrame = parseInt(e.target.value)
        e.stopPropagation()
      }
    }),
    $el({ content: 'to' }),
    exportInputCustomEnd = $el({
      tag: 'input',
      className: 'input input-boxed rounded text-center flex-grow',
      value: timelineEndFrame,
      type: 'number',
      min: 1,
      max: totalFramesCount - 1,
      onchange: (e) => {
        exportEndFrame = parseInt(e.target.value)
        e.stopPropagation()
      },
      oninput: (e) => {
        exportEndFrame = parseInt(e.target.value)
        e.stopPropagation()
      }
    }),
  ], { className: 'my-1' })

  const exportRangeForm = $el({
    tag: 'form',
    className: 'my-2 w-full',
    content: [
      $d([
        $el({
          tag: 'input',
          type: 'radio',
          id: 'export-range-all',
          name: 'export-range',
          value: 'all',
          checked: true,
        }),
        $el({
          tag: 'label',
          className: 'ml-1',
          for: 'export-range-all',
          content: 'Export all',
        })
      ]),
      $d([
        $el({
          tag: 'input',
          type: 'radio',
          id: 'export-range-zoomed',
          name: 'export-range',
          value: 'zoomed',
        }),
        $el({
          tag: 'label',
          className: 'ml-1',
          for: 'export-range-zoomed',
          content: 'Export currently zoomed area',
        })
      ]),
      contentExportZoomed,
      $d([
        $el({
          tag: 'input',
          type: 'radio',
          id: 'export-range-custom',
          name: 'export-range',
          value: 'custom',
        }),
        $el({
          tag: 'label',
          className: 'ml-1',
          for: 'export-range-custom',
          content: 'Export custom range',
        })
      ]),
      contentExportCustom,
    ],
    onchange: (e) => {
      const v = e.target.value
      exportMode = v
      if (v === 'all') {
        contentExportZoomed.style.display = 'none'
        contentExportCustom.style.display = 'none'
      } else if (v === 'zoomed') {
        contentExportZoomed.style.display = 'flex'
        contentExportCustom.style.display = 'none'
      } else if (v === 'custom') {
        contentExportZoomed.style.display = 'none'
        contentExportCustom.style.display = 'flex'
      }
    }
  })

  contentExportZoomed.style.display = 'none'
  contentExportCustom.style.display = 'none'

  const CheckboxExportBeautify = $el({
    tag: 'input',
    type: 'checkbox',
    className: 'my-1',
    id: 'export-beautify',
    onchange: (e) => {
      exportBeautify = e.target.checked
    },
  })
  if (exportBeautify)
    CheckboxExportBeautify.checked = true

  const CheckboxExportCropMargins = $el({
    tag: 'input',
    type: 'checkbox',
    className: 'my-1',
    id: 'export-crop-margins',
    onchange: (e) => {
      exportCropMargins = e.target.checked
    },
  })
  if (exportCropMargins)
    CheckboxExportCropMargins.checked = true

  const ExportScaleInput = $el({
    tag: 'input',
    id: 'export-scale',
    className: 'input input-boxed p-1 rounded text-sm my-2',
    value: exportVideoScale,
    onchange: (e) => {
      exportVideoScale = parseFloat(e.target.value)
    }
  })

  return $Modal({
    title: 'Export frames',
    width: 600,
    height: 300,
    content: $el({
      content: [
        $el({ content: 'Pick your export options:' }),
        // $d([
        //   CheckboxExportBeautify,
        //   $el({
        //     tag: 'label',
        //     for: 'export-beautify',
        //     content: 'Beautify (= as a single-layer merged video)',
        //     className: 'ml-1',
        //   }),
        // ]),
        $d([
          CheckboxExportCropMargins,
          $el({
            tag: 'label',
            for: 'export-crop-margins',
            content: 'Crop margins',
            className: 'ml-1',
          }),
        ]),
        $d([
          ExportScaleInput,
          $el({
            tag: 'label',
            for: 'export-scale',
            content: 'Video Scale',
            className: 'ml-1',
          }),
        ]),
        exportRangeForm,
      ]
    }),
    confirmText: 'Export',
    onCancel: () => toggleExportModal(false),
    onConfirm: () => {
      setRecording(false)
      setExporting(true)
      toggleExportModal(false)
    },
  })
}

function createMainArea() {
  videoContainer = createVideoPlayer()

  recordingOverlay = $el({
    className: 'overlay absolute left-0 top-0 w-full h-full border-4 border-red-700 pointer-events-none',
  })

  testingOverlay = $el({
    className: 'overlay absolute left-0 top-0 w-full h-full border-4 border-blue-700 pointer-events-none',
  })

  testingCanvasControls = $el({
    id: 'test-canvas-controls',
    className: 'p-4 bg-gray-200 dark:bg-gray-700 grid-flow-col auto-cols-auto items-center',
    content: [
      $el({
        content: 'This is a test canvas to try out the different brushes and controls. Have fun! :)'
      }),
      $el({
        tag: 'button',
        className: 'bg-gray-400 text-white p-2 rounded focus:outline-none hover:bg-gray-300',
        content: 'Clear canvas',
        onclick: clearCanvas
      })
    ]
  })

  exportingOverlay = $el({
    className: 'absolute z-10 left-0 top-0 w-screen h-screen opacity-90 bg-gray-100 grid place-items-center',
    content: $el({
      className: 'text-center',
      content: [
        $el({ content: 'Exporting...' }),
        exportMessage = $el({ content: '' }),
      ]}
    )
  })

  timeline = createTimeline()

  canvases = [createCanvas()]


  mainArea = $el({
    id: 'main-area',
    content: [
      videoContainer,
      recordingOverlay,
      testingOverlay,
      testingCanvasControls,
      exportingOverlay,
      timeline,
      canvases[0],
      hiddenCanvas = createCanvas(-1),
    ]
  })

  hiddenCanvas.style.display = 'none'
  hiddenCanvas.style.pointerEvents = 'none'

  return mainArea
}

async function createToolbar() {
  const TGeneralSettingsSliderVideoPadding = $Slider({
    label: 'Video margins',
    className: 'flex items-center justify-between',
    initialValue: videoPadding,
    min: 0,
    max: 100,
    step: 5,
    onchange: setVideoPadding,
    oninput: setVideoPadding,
    inline: true
  })
  const TGeneralSettingsSliderPlaybackSpeed = $Slider({
    label: 'Playback speed',
    className: 'mt-1 flex items-center justify-between',
    initialValue: playbackSpeed,
    min: 0.1,
    max: 1,
    step: 0.05,
    onchange: setPlaybackSpeed,
    inline: true
  })
  const TGeneralSettingsCBoxFadeOut = $Checkbox({
    className: 'mt-1',
    label: 'Fade',
    initialValue: brushFade,
    onchange: setBrushFade
  })
  brushFadeCheckbox = TGeneralSettingsCBoxFadeOut.querySelector('input[type="checkbox"]')
  const TGeneralSettingsSliderFadeSpeed = $Slider({
    label: 'Fade speed',
    className: 'mt-1 flex items-center justify-between',
    initialValue: brushFadingSpeed,
    min: 1,
    max: 6,
    step: 0.2,
    onchange: setBrushFadingSpeed,
    inline: true
  })
  brushFadingSpeedSlider = TGeneralSettingsSliderFadeSpeed.querySelector('input[type="range"]')

  const TBrushPresets = $el({
    content: [
      $d('Preset', 'text-sm'),
      $r([
        brushPresetsSelect = $el({
          tag: 'select',
          className: 'input-boxed rounded flex-grow text-sm p-1',
          style: { 'min-width': '120px' },
          content: [
            $el({
              tag: 'option',
              value: -1,
              content: '(Choose a preset)',
              selected: true,
              disabled: true
            })
          ].concat(
            presets.map((preset) => $el({
              tag: 'option',
              value: preset.key,
              content: preset.name,
            }))
          ),
          onchange: async (e) => {
            await usePreset(parseInt(e.target.value))
          }
        }),
        $b({ content: '+', onclick: () => toggleNewPresetNamePanel(true), className: 'py-0' }),
        $b({ content: '-', onclick: () => toggleRemovePresetConfirm(true), className: 'py-0' }),
        $b({
          content: $Icon({
            width: 16,
            height: 16,
            path: 'M4 4v5h.582m15.356 2A8.001 8.001 0 004.582 9m0 0H9m11 11v-5h-.581m0 0a8.003 8.003 0 01-15.357-2m15.357 2H15',
          }),
          onclick: () => updatePreset(),
          className: 'py-1'
        }),
        brushNewPresetNamePanel = $el({
          id: 'new-preset-name-panel',
          className: 'bg-gray-100 p-2 rounded w-full',
          content: [
            brushNewPresetNameInput = $el({
              tag: 'input',
              className: 'text-sm rounded w-full p-1',
              placeholder: 'Enter preset name...',
              type: 'text',
              onkeypress: (e) => {
                if (e.key === 'Enter') {
                  addPreset(e.target.value)
                  e.target.value = ''
                  toggleNewPresetNamePanel(false)
                }
              }
            }),
            $el({ content: '(Press &lt;Enter&gt; to validate, &lt;Esc&gt; to cancel)', className: 'text-xs italic opacity-50 mt-1' })
          ]
        }),
      ], { className: 'relative my-2' })
    ]
  })

  brushColorInput = $el({
    tag: 'input',
    className: 'input input-boxed p-1 rounded text-sm my-2',
    value: brushColors[0],
    onchange: (e) => {
      const color = e.target.value
      setBrushColor(color, currentLayer, true, true, false)
    }
  })
  const TBrushSettingsColorPicker = $el({ id: 'brush-color-picker' })
  
  const TColorSwatchesGrid = $el({
    label: 'Color swatches',
    className: 'my-2',
    content: colorSwatchesGrid = $el({
      id: 'color-swatches-grid',
      content: [
        $b({
          content: '+',
          className: 'py-0',
          style: { width: '28px', height: '28px' },
          onclick: addColorSwatch,
        })
      ]
    })
  })

  brushSizeSlider = $EditableSlider({
    label: 'Size',
    className: 'flex items-center justify-between',
    initialValue: initialBrushSize,
    initialMin: 3,
    initialMax: 120,
    oninput: setBrushSize,
    inline: true
  })
  sizeRandomSlider = $Slider({
    label: 'Size random',
    className: 'mt-1 flex items-center justify-between',
    initialValue: 0,
    min: 0,
    max: 1,
    oninput: setSizeRandomness,
    inline: true
  })
  angleRandomSlider = $Slider({
    label: 'Angle random',
    className: 'mt-1 flex items-center justify-between',
    initialValue: 0,
    min: 0,
    max: 1,
    oninput: setSizeRandomness,
    inline: true
  })
  
  const TBrushSettingsGridStyles = $Grid({
    label: 'Style',
    className: 'mt-1 flex items-center',
    content: (await (async () => {
      brushTypeButtons = []
      const imgUrls = await Promise.all(brushes.map((brush) => {
        const imgUrl = `assets/brushes_black/${brush.image}`
        return new Promise((resolve) => {
          imageExists(imgUrl, (exists) => {
            exists ? resolve(imgUrl) : resolve(null)
          })
        })
      }))
      let brushIndex = 0
      for (let { key: type, name } of brushes) {
        const imgUrl = imgUrls[brushIndex]
        const img = imgUrl
          ? $el({
              tag: 'img',
              src: imgUrl,
              alt: `brush-${type}`,
              width: '32px',
              height: '32px',
              className: 'max-w-none',
            })
          : $svg({
              tag: 'svg',
              width: 32,
              height: 32,
              xmlns: svgns,
              viewBox: '0 0 24 24',
              fill: 'black',
              content: $svg({
                tag: 'circle',
                cx: 12,
                cy: 12,
                r: 10,
              })
            })
        const btn = $el({
          tag: 'button',
          className: 'focus:outline-none opacity-50',
          'data-type': type,
          title: name,
          content: img,
          onclick: async () => {
            await setBrushType(type, null, false, true)
          }
        })
        brushTypeButtons.push(btn)
        brushIndex++
      }
      return brushTypeButtons
    })())
  })

  const TTop = $el({
    className: 'flex flex-col gap-2',
    content: [
      $el({ id: 'version', content: `v ${VERSION}` }),
      $Accordion({
        header: 'General settings',
        content: [
          TGeneralSettingsSliderVideoPadding,
          TGeneralSettingsSliderPlaybackSpeed,
          TGeneralSettingsCBoxFadeOut,
          TGeneralSettingsSliderFadeSpeed,
        ]
      }),
      $Accordion({
        header: 'Brush configuration',
        content: [
          TBrushPresets,
          $el({
            className: 'flex justify-center items-center',
            content: [
              brushColorInput,
              $b({
                className: 'ml-1',
                style: { height: '32px' },
                content: $Icon({
                  width: 16,
                  height: 16,
                  path: 'M8 5H6a2 2 0 00-2 2v12a2 2 0 002 2h10a2 2 0 002-2v-1M8 5a2 2 0 002 2h2a2 2 0 002-2M8 5a2 2 0 012-2h2a2 2 0 012 2m0 0h2a2 2 0 012 2v3m2 4H10m0 0l3-3m-3 3l3 3'
                }),
                onclick: () => {
                  brushColorInput.select()
                  document.execCommand('copy')
                  brushColorInput.blur()
                  toast('Copied to clipboard!')
                }
              })
            ]
          }),
          TBrushSettingsColorPicker,
          TColorSwatchesGrid,
          brushSizeSlider,
          // sizeRandomSlider,
          // angleRandomSlider,
          TBrushSettingsGridStyles,
        ]
      }),
    ],
  })

  btnRecording = $b({
    onclick: () => setRecording(!recording),
    color: 'red',
    disabled: true,
    content: $svg({
      tag: 'svg',
      width: 16,
      height: 16,
      xmlns: svgns,
      viewBox: '0 0 24 24',
      fill: 'currentColor',
      content: $svg({ tag: 'circle', cx: 12, cy: 12, r: 10 })
    })
  })

  btnTesting = $b({
    onclick: () => setTesting(!testing),
    color: 'blue',
    content: $Icon({
      width: 16,
      height: 16,
      stroked: false,
      path: [
        'M8 3a1 1 0 011-1h2a1 1 0 110 2H9a1 1 0 01-1-1z',
        'M6 3a2 2 0 00-2 2v11a2 2 0 002 2h8a2 2 0 002-2V5a2 2 0 00-2-2 3 3 0 01-3 3H9a3 3 0 01-3-3z',
      ]
    })
  })

  btnExport = $b({
    onclick: () => toggleExportModal(true),
    disabled: true,
    content: $Icon({
      width: 16,
      height: 16,
      path: 'M17 16v2a2 2 0 01-2 2H5a2 2 0 01-2-2v-7a2 2 0 012-2h2m3-4H9a2 2 0 00-2 2v7a2 2 0 002 2h10a2 2 0 002-2V7a2 2 0 00-2-2h-1m-1 4l-3 3m0 0l-3-3m3 3V3'
    })
  })

  btnClearPenActions = $b({
    onclick: () => toggleClearPenActionsConfirm(true),
    disabled: true,
    content: $Icon({
      width: 16,
      height: 16,
      path: 'M19 7l-.867 12.142A2 2 0 0116.138 21H7.862a2 2 0 01-1.995-1.858L5 7m5 4v6m4-6v6m1-10V4a1 1 0 00-1-1h-4a1 1 0 00-1 1v3M4 7h16'
    })
  })

  btnLoadSession = $b({
    onclick: loadSession,
    disabled: true,
    content: $Icon({
      width: 16,
      height: 16,
      path: 'M4 16v1a3 3 0 003 3h10a3 3 0 003-3v-1m-4-8l-4-4m0 0L8 8m4-4v12'
    })
  })

  btnSaveSession = $b({
    onclick: saveSession,
    disabled: true,
    content: $Icon({
      width: 16,
      height: 16,
      path: 'M4 16v1a3 3 0 003 3h10a3 3 0 003-3v-1m-4-4l-4 4m0 0l-4-4m4 4V4'
    })
  })

  // const TButtonsThemeSwitch = $el({
  //   className: 'flex gap-2 items-center',
  //   content: [
  //     $svg({
  //       tag: 'svg',
  //       width: 16,
  //       height: 16,
  //       xmlns: svgns,
  //       viewBox: '0 0 24 24',
  //       fill: 'none',
  //       stroke: 'currentColor',
  //       content: $svg({
  //         tag: 'path',
  //         'stroke-linecap': 'round',
  //         'stroke-linejoin': 'round',
  //         'stroke-width': 2,
  //         d: 'M12 3v1m0 16v1m9-9h-1M4 12H3m15.364 6.364l-.707-.707M6.343 6.343l-.707-.707m12.728 0l-.707.707M6.343 17.657l-.707.707M16 12a4 4 0 11-8 0 4 4 0 018 0z',
  //       })
  //     }),
  //     $Switch({
  //       initialValue: darkThemeIsOn,
  //       name: 'dark-theme-switch',
  //       onchange: setDarkTheme,
  //     }),
  //     $svg({
  //       tag: 'svg',
  //       width: 16,
  //       height: 16,
  //       xmlns: svgns,
  //       viewBox: '0 0 24 24',
  //       fill: 'none',
  //       stroke: 'currentColor',
  //       content: $svg({
  //         tag: 'path',
  //         'stroke-linecap': 'round',
  //         'stroke-linejoin': 'round',
  //         'stroke-width': 2,
  //         d: 'M20.354 15.354A9 9 0 018.646 3.646 9.003 9.003 0 0012 21a9.003 9.003 0 008.354-5.646z',
  //       })
  //     })
  //   ]
  // })

  const TBottom = $el({
    className: 'flex gap-2 items-center',
    content: [
      btnRecording,
      btnTesting,
      btnExport,
      btnLoadSession,
      btnSaveSession,
      btnClearPenActions,
      // TButtonsThemeSwitch,
    ],
  })

  const T = $el({
    id: 'toolbar',
    className: 'p-4 w-full bg-white dark:bg-gray-800 dark:text-white shadow-md select-none flex flex-col justify-between',
    content: [ TTop, TBottom ],
  })

  return T
}

async function createHTML() {
  const container = document.getElementById('container')

  const toolbar = await createToolbar()
  const mainArea = createMainArea()

  removeLayerConfirm = $Modal({
    title: 'Remove layer',
    content: 'Are you sure you want to remove this layer? All recorded actions will be lost!',
    cancelText: 'No',
    confirmText: 'Yes',
    onCancel: () => toggleRemoveLayerConfirm(false),
    onConfirm: async () => {
      await removeLayer(layerToRemove)
      toggleRemoveLayerConfirm(false)
    },
  })

  changeVideoConfirm = $Modal({
    title: 'Load video',
    content: 'Are you sure you want to load another video? This will remove all recorded actions!',
    cancelText: 'No',
    confirmText: 'Yes',
    onCancel: () => toggleChangeVideoConfirm(false),
    onConfirm: async () => {
      await changeVideo()
      toggleChangeVideoConfirm(false)
    },
  })

  removePresetConfirm = $Modal({
    title: 'Remove preset',
    content: 'Are you sure you want to remove this preset?',
    cancelText: 'No',
    confirmText: 'Yes',
    onCancel: () => toggleRemovePresetConfirm(false),
    onConfirm: async () => {
      await removePreset(parseInt(brushPresetsSelect.value))
      toggleRemovePresetConfirm(false)
    },
  })

  clearPenActionsConfirm = $Modal({
    title: 'Clear recorded actions',
    content: 'Are you sure you want to clear all the actions you recorded?',
    cancelText: 'No',
    confirmText: 'Yes',
    onCancel: () => toggleClearPenActionsConfirm(false),
    onConfirm: () => {
      clearPenActions()
      toggleClearPenActionsConfirm(false)
    },
  })

  exportModal = createExportModal()

  container.appendChild(mainArea)
  container.appendChild(toolbar)
  container.appendChild(removeLayerConfirm)
  container.appendChild(changeVideoConfirm)
  container.appendChild(removePresetConfirm)
  container.appendChild(clearPenActionsConfirm)
  container.appendChild(exportModal)

  toastsHolder = $el({ id: 'toasts-holder' })
  container.appendChild(toastsHolder)
}

/* ================= */

function toggleNewPresetNamePanel(show) {
  brushNewPresetNamePanel.style.display = show ? 'block' : 'none'
  if (show) {
    brushNewPresetNameInput.focus()
    window.onkeydown = (e) => {
      if (e.key === 'Escape') {
        brushNewPresetNameInput.value = ''
        toggleNewPresetNamePanel(false)
      }
    }
  } else {
    window.onkeypress = undefined
  }
}

function toggleRemoveLayerConfirm(show) {
  removeLayerConfirm.style.display = show ? 'block' : 'none'
}

function toggleChangeVideoConfirm(show) {
  changeVideoConfirm.style.display = show ? 'block' : 'none'
}

function toggleRemovePresetConfirm(show) {
  removePresetConfirm.style.display = show ? 'block' : 'none'
}

function toggleClearPenActionsConfirm(show) {
  clearPenActionsConfirm.style.display = show ? 'block' : 'none'
}

function toggleExportModal(show) {
  exportInputZoomedStart.value = timelineStartFrame
  exportInputZoomedEnd.value = timelineEndFrame
  exportInputCustomStart.value = timelineStartFrame
  exportInputCustomEnd.value = timelineEndFrame
  exportStartFrame = timelineStartFrame
  exportEndFrame = timelineEndFrame
  exportModal.style.display = show ? 'block' : 'none'
}

async function changeVideo(inputRef = null) {
  if (inputRef === null) inputRef = currentVideoFileInput
  await loadVideo(inputRef)
  setupTimeline()
}

function setCanvasSize(mode) {
  canvasMode = mode
  for (const canvas of canvases) {
    if (mode === 'video') {
      const { width: w, height: h } = videoPlayer.getBoundingClientRect()
      canvas.width = w
      canvas.height = h
    } else if (mode === 'full') {
      canvas.width = window.innerWidth - Math.max(window.innerWidth * 0.25, 150)
      canvas.height = window.innerHeight - 80
    }
  }
}

function setupTimeline() {
  timelineTotalFramesCountLabel.innerText = `/ ${totalFramesCount - 1}`
  timelineStartFrameInput.min = 0
  timelineStartFrameInput.max = totalFramesCount - 2
  timelineStartFrameInput.value = 0
  timelineEndFrameInput.min = 1
  timelineEndFrameInput.max = totalFramesCount - 1
  timelineEndFrameInput.value = totalFramesCount - 1
  timelineCurrentFrameInput.setAttribute('max', totalFramesCount - 1)
  timelineCurrentFrameInput.value = currentVideoFrame
}

let minGap = 1
function updateTimelineZoomIndicatorLeftHandle() {
  const leftVal = parseInt(timelineZoomIndicatorLeftHandle.value)
  const rightVal = parseInt(timelineZoomIndicatorRightHandle.value)
  if (rightVal - leftVal <= minGap) {
    timelineZoomIndicatorLeftHandle.value = rightVal - minGap
  }
  fillTimelineZoomIndicator()

  setTimelineStartFrame(parseInt(timelineZoomIndicatorLeftHandle.value))
}
function updateTimelineZoomIndicatorRightHandle() {
  const leftVal = parseInt(timelineZoomIndicatorLeftHandle.value)
  const rightVal = parseInt(timelineZoomIndicatorRightHandle.value)
  if (rightVal - leftVal <= minGap) {
    timelineZoomIndicatorRightHandle.value = leftVal + minGap
  }
  fillTimelineZoomIndicator()

  setTimelineEndFrame(parseInt(timelineZoomIndicatorRightHandle.value))
}
function fillTimelineZoomIndicator() {
  let sliderMaxValue = timelineZoomIndicatorLeftHandle.max
  const leftVal = parseInt(timelineZoomIndicatorLeftHandle.value)
  const rightVal = parseInt(timelineZoomIndicatorRightHandle.value)
  percent1 = (leftVal / sliderMaxValue) * 100
  percent2 = (rightVal / sliderMaxValue) * 100
  timelineZoomIndicator.style.background = `linear-gradient(to right,
    transparent ${percent1}%,
    #9ca3af ${percent1}%,
    #9ca3af ${percent2}%,
    transparent ${percent2}%)`

  const r = timelineBackground.getBoundingClientRect().width / totalFramesCount
  const x1 = leftVal * r
  const x2 = rightVal * r
  timelineZoomIndicatorDragger.style.left = `calc(${x1}px + 8px)`
  timelineZoomIndicatorDragger.style.width = `calc(${x2 - x1}px - 16px)`
}

function updateTimeline() {
  timelineZoom = totalFramesCount / (timelineEndFrame - timelineStartFrame)
  updateTimelineStrokes()
  updateTimelineConfigs()
  updateCurrentFrameMarker()
  updateTimelineMarkers()
}

function updateCurrentFrameMarker() {
  const { width: w } = timelineBackground.getBoundingClientRect()
  const ratio1 = w / (timelineEndFrame + 1 - timelineStartFrame)
  const x1 = ratio1 * (currentVideoFrame - timelineStartFrame)
  timelineCurrentFrame.style.left = `${Math.round(x1)}px`
  const ratio2 = w / totalFramesCount
  const x2 = ratio2 * currentVideoFrame
  timelineZoomIndicatorCurrentFrame.style.left = `${Math.round(x2)}px`
}

function updateTimelineMarkers() {
  if (timelineEndFrame === -1)
    return

  const { width: wBg } = timelineBackground.getBoundingClientRect()
  const { width: wDragger } = timelineZoomIndicatorDragger.getBoundingClientRect()
  const ratio = wDragger / wBg

  removeChildren(timelineReferenceFramesWrapper)

  // main markers
  let step = 100
  for (let i = ceilToClosest(timelineStartFrame, step); i < timelineEndFrame; i += step) {
    appendContent(
      timelineReferenceFramesWrapper,
      $el({
        className: 'frame',
        style: { left: `${(i - timelineStartFrame) / ratio}px` },
      }),
    )
  }
  // small markers (if zoomed enough)
  if (timelineZoom > 2) {
    step = 10
    for (let i = ceilToClosest(timelineStartFrame, step); i < timelineEndFrame; i += step) {
      appendContent(
        timelineReferenceFramesWrapper,
        $el({
          className: 'frame-light',
          style: { left: `${(i - timelineStartFrame) / ratio}px` },
        }),
      )
    }
  }
}

function updateTimelineStrokes() {
  const { width: w } = timelineBackground.getBoundingClientRect()
  const ratio = w * timelineZoom / totalFramesCount

  for (const stroke of strokes) {
    if (stroke.start > timelineEndFrame || stroke.end < timelineStartFrame)
      stroke.domElement.style.display = 'none'
    else
      stroke.domElement.style.display = 'block'

    const x1 = Math.max(0, ratio * (stroke.start - timelineStartFrame))
    const x2 = Math.min(w, ratio * (stroke.end - timelineStartFrame))
    stroke.domElement.style.left = `${x1}px`
    stroke.domElement.style.width = `${x2 - x1}px`
  }
}

function updateTimelineConfigs() {
  const { width: w } = timelineBackground.getBoundingClientRect()
  const ratio = w * timelineZoom / totalFramesCount

  for (const config of configs) {
    if (config.frame > timelineEndFrame || config.frame < timelineStartFrame)
      config.domElement.style.display = 'none'
    else
      config.domElement.style.display = 'block'

    const x = ratio * (config.frame - timelineStartFrame)
    config.domElement.style.left = `${x}px`
  }
}

function selectStroke(strokeId) {
  if (selectedStroke !== null) {
    selectedStroke.domElement.classList.remove('selected')
    selectedStroke = null
  } else if (selectedConfig !== null) {
    selectedConfig.domElement.classList.remove('selected')
    selectedConfig = null
  }
  selectedStroke = strokes.find((stroke) => stroke.id === strokeId)
  selectedStroke.domElement.classList.add('selected')
}

function selectConfig(configId) {
  if (selectedStroke !== null) {
    selectedStroke.domElement.classList.remove('selected')
    selectedStroke = null
  } else if (selectedConfig !== null) {
    selectedConfig.domElement.classList.remove('selected')
    selectedConfig = null
  }
  selectedConfig = configs.find((config) => config.id === configId)
  selectedConfig.domElement.classList.add('selected')
}

function removeChildren(element) {
  while (element.firstChild)
    element.removeChild(element.firstChild)
}
