function saveSession() {
  const data = {
    actions,
    swatches,
    brushColors,
    brushSizes,
    brushTypes,
    brushFade,
    brushFadingSpeed,
    nLayers: Object.keys(brushColors).length,
    currentLayer,
  }

  const content = encodeURIComponent(JSON.stringify(data))
  const link = document.createElement('a')
  link.download = 'session.json'
  link.href = `data:text/json;charset=utf-8,${content}`
  link.click()
  link.remove()
}

function loadSession() {
  const input = document.createElement('input')
  input.type = 'file'
  input.hidden = true
  input.onchange = () => {
    const reader = new FileReader()
    const file = input.files[0]
    reader.readAsText(file)
    reader.onload = async () => {
      const content = reader.result
      const data = JSON.parse(decodeURIComponent(content))

      loadSwatches(data.swatches)

      for (let i = 0; i < data.nLayers - 1; i++)
        await addLayer(false)
      await setCurrentLayer(data.currentLayer)
      brushColors = data.brushColors
      brushSizes = data.brushSizes
      brushTypes = data.brushTypes
      setBrushFade(data.brushFade, true)
      setBrushFadingSpeed(data.brushFadingSpeed, true)

      if (Object.keys(data.actions).length > 0) {
        for (const [frame, actions] of Object.entries(data.actions)) {
          for (const action of actions) {
            action.frame = frame
            pushPenAction(action)
          }
        }
      }
      else {
        actions = {}
      }
      await setCurrentVideoFrame(0, true, true, true)
    }
  }
  input.click()
  input.remove()
}
