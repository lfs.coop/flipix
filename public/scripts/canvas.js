var
  mousePressed = { 0: false },
  lastX = {},
  lastY = {},
  fadingIntervals = { 0: null },
  ctx

function downHandler(e, layer = null) {
  if (layer === null) layer = currentLayer
  mousePressed[layer] = true
  draw(e.pageX, e.pageY, e.pressure, false, layer)
  if (recording && layer === currentLayer)
    pushPenAction({
      type: 'down',
      layer,
      event: {
        pageX: e.pageX,
        pageY: e.pageY,
        pressure: e.pressure,
      }
    })
}

function moveHandler(e, layer = null) {
  if (layer === null) layer = currentLayer
  if (mousePressed[layer]) {
    draw(e.pageX, e.pageY, e.pressure, true, layer)
    if (recording && layer === currentLayer)
      pushPenAction({
        type: 'move',
        layer,
        event: {
          pageX: e.pageX,
          pageY: e.pageY,
          pressure: e.pressure,
        }
      })
  }
}

function upHandler(e, layer = null) {
  if (layer === null) layer = currentLayer
  mousePressed[layer] = false
  if (recording && layer === currentLayer)
    pushPenAction({ type: 'up', layer })
}

function leaveHandler(e) {
  const c = e.target
  const l = parseInt(c.id.replace(/draw-canvas-/, ''))
  if (l === currentLayer) {
    mousePressed[l] = false
    if (recording)
      pushPenAction({ type: 'up', layer: currentLayer })
  }
}

function draw(x, y, pressure, isDown, layer) {
  if (isDown) {
    ctx = canvases[layer].getContext('2d')
    ctx.lineJoin = ctx.lineCap = 'round'

    const start = { x: lastX[layer], y: lastY[layer] }
    const end = { x, y }
    
    const distance = parseInt(Trig.distanceBetween2Points(start, end))
    const angle = Trig.angleBetween2Points(start, end)

    const config = brushData[brushTypes[layer]]
    const m = (Math.random() < (config.sizeRandomness || 0)) ? (0.5 + Math.random() * 0.5) : 1
    const realW = config.imgData.width * brushSizeRatios[layer] * (config.usesPressure ? pressure : 1) * m
    const realH = config.imgData.height * brushSizeRatios[layer] * (config.usesPressure ? pressure : 1) * m
    const halfBrushW = realW / 2
    const halfBrushH = realH / 2
    
    let px, py
    for (let z = 0; (z <= distance || z == 0); z++) {
      const a = (Math.random() < (config.angleRandomness || 0)) ? (Math.random() * 0.2 * angle) : 0
      px = start.x + (Math.sin(angle + a) * z) - halfBrushW
      py = start.y + (Math.cos(angle + a) * z) - halfBrushH
      ctx.save()
      ctx.fillStyle = brushColors[layer]
      ctx.drawImage(config.imgData, px, py, realW, realH)
      ctx.globalCompositeOperation = 'source-atop'
      ctx.globalAlpha = 1
      ctx.fillRect(px, py, realW, realH)
      ctx.restore()
    }
  }
  lastX[layer] = x
  lastY[layer] = y
}

function replayStrokes(strokes) {
  clearCanvas()
  if (strokes.length === 0) return
  const layer = strokes[0].layer
  mousePressed[layer] = true
  for (const stroke of strokes) {
    replayStroke(stroke, stroke.layer)
  }
  mousePressed[layer] = false
}

function replayStroke(stroke, layer) {
  draw(stroke.initialPosition.x, stroke.initialPosition.y, stroke.initialPressure, false, layer)
  for (const move of stroke.moves) {
    draw(move.x, move.y, move.pressure, true, layer)
  }
}

function clearCanvas() {
  for (const canvas of canvases) {
    const ctx = canvas.getContext('2d')
    ctx.setTransform(1, 0, 0, 1, 0, 0)
    ctx.clearRect(0, 0, ctx.canvas.width, ctx.canvas.height)
  }
}

function setCanvasFade(canvas = null) {
  const checkedCanvases = canvas === null ? canvases : [canvas]
  for (const canvas of checkedCanvases) {
    const layer = parseInt(canvas.id.replace(/draw-canvas-/, ''))
    if (!brushFade) {
      if (fadingIntervals[layer] !== null) {
        clearInterval(fadingIntervals[layer])
        fadingIntervals[layer] = null
      }
      continue
    }
  
    if (fadingIntervals[layer] !== null)
      clearInterval(fadingIntervals[layer])

    const ctx = canvas.getContext('2d')
    const
      width = canvas.width,
      height = canvas.height
    fadingIntervals[layer] = setInterval(() => {
      if (!playing) return
      ctx.save()
      ctx.globalCompositeOperation = 'destination-out'
      ctx.fillStyle = 'rgba(255, 255, 255, 0.1)'
      ctx.fillRect(0, 0, width, height)
      ctx.restore()
    }, 100 / brushFadingSpeed)
  }
}

function removeCanvas(layer) {
  if (fadingIntervals[layer] !== null)
    clearInterval(fadingIntervals[layer])

  document.getElementById(`draw-canvas-${layer}`).remove()
}
