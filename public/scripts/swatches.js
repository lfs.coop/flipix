let swatches = []

function loadSwatches(s) {
  swatches = s
  for (const color of swatches) {
    const button = $el({
      className: 'color-swatch',
      style: { 'background-color': color },
      onclick: () => setBrushColor(color, currentLayer, true, true)
    })
    appendContent(colorSwatchesGrid, button)
  }
}

function addColorSwatch() {
  const color = brushColorPicker.color.hexString
  swatches.push(color)
  const button = $el({
    className: 'color-swatch',
    style: { 'background-color': color },
    onclick: () => setBrushColor(color, currentLayer, true, true)
  })
  appendContent(colorSwatchesGrid, button)
}
