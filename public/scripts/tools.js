function imageExists(url, callback) {
  var img = new Image();
  img.onload = function() { callback(true); };
  img.onerror = function() { callback(false); };
  img.src = url;
}

function toast(content, color = 'indigo') {
  const t = $Toast({ content, color })
  appendContent(toastsHolder, t)
  setTimeout(() => {
    t.remove()
  }, 3000)
}
