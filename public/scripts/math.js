const Trig = {
  distanceBetween2Points: function ( point1, point2 ) {

      var dx = point2.x - point1.x;
      var dy = point2.y - point1.y;
      return Math.sqrt( Math.pow( dx, 2 ) + Math.pow( dy, 2 ) );
  },

  angleBetween2Points: function ( point1, point2 ) {

      var dx = point2.x - point1.x;
      var dy = point2.y - point1.y;
      return Math.atan2( dx, dy );
  }
}

function ceilToClosest(value, step) {
  step || (step = 1.0)
  var inv = 1.0 / step
  return Math.ceil(value * inv) / inv
}
