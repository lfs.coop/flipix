var presets = []

function loadPresets() {
  const storageData = localStorage.getItem('__flipix__presets')
  if (!storageData) return []
  return JSON.parse(storageData)
}

function savePresets() {
  localStorage.setItem('__flipix__presets', JSON.stringify(presets))
}

function addPreset(name) {
  const preset = {
    key: presets.length,
    name,
    color: brushColors[currentLayer],
    size: brushSizes[currentLayer],
    type: brushTypes[currentLayer],
  }
  presets.push(preset)
  appendContent(brushPresetsSelect, $el({
    tag: 'option',
    value: preset.key,
    content: preset.name,
  }))
  brushPresetsSelect.value = preset.key

  savePresets()
}

function updatePreset() {
  if (brushPresetsSelect.value === '-1') return
  const idx = presets.findIndex((p) => p.key === parseInt(brushPresetsSelect.value))
  const preset = {
    ...presets[idx],
    color: brushColors[currentLayer],
    size: brushSizes[currentLayer],
    type: brushTypes[currentLayer],
  }
  presets[idx] = preset
  savePresets()
}

async function removePreset(presetKey) {
  const presetIdx = presets.findIndex((p) => p.key === presetKey)
  if (presetIdx !== -1) {
    presets.splice(presetIdx, 1)
    brushPresetsSelect.querySelector(`option[value="${presetKey}"]`).remove()
    savePresets()
  }
  brushPresetsSelect.value = 0
  await usePreset(0)
}

async function usePreset(presetKey) {
  const preset = presets.find((p) => p.key === presetKey)
  setBrushColor(preset.color, currentLayer, true, true)
  setBrushSize(preset.size, currentLayer, true, true)
  await setBrushType(preset.type, currentLayer, true, true)
}
