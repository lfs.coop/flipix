const FPS = 24
var checkFrameInterval

function startCheckFrameInterval() {
  setPlaying(true)
  checkFrameInterval = setInterval(() => {
    setCurrentVideoFrame(Math.floor(videoPlayer.currentTime * FPS), false)
  }, FPS)
}

function stopCheckFrameInterval() {
  setPlaying(false)
  clearInterval(checkFrameInterval)
  checkFrameInterval = null
}

function loadVideo(fileInput) {
  return new Promise((resolve) => {
    const reader = new FileReader()
    const file = fileInput.files[0]
    reader.onload = (e) => {
      setVideo(e.target.result)
      videoPlayer.querySelector('source').src = URL.createObjectURL(file)
      videoPlayer.muted = videoMuted
      videoPlayer.onended = () => {
        clearCanvas()
        videoPlayer.currentTime = 0
        videoPlayer.play()
      }
      videoPlayer.onloadedmetadata = () => {
        currentVideoFrame = 0
        totalFramesCount = Math.floor(videoPlayer.duration * FPS)

        // setup canvas
        setCanvasSize('video')

        document.activeElement = null
        timelineZoomIndicatorLeftHandle.max = totalFramesCount - 1
        timelineZoomIndicatorRightHandle.max = totalFramesCount - 1
        timelineZoomIndicatorRightHandle.value = totalFramesCount - 1
        fillTimelineZoomIndicator()

        // setup timeline
        setTimelineStartFrame(0)
        setTimelineEndFrame(totalFramesCount - 1)

        // register initial configuration
        pushPenAction({
          type: 'config',
          brushColor: brushColors[0],
          brushSize: brushSizes[0],
          brushType: brushTypes[0],
          layer: 0,
        })

        document.getElementById('video-placeholder-file-input-button').style.display = 'none'
        videoPlayer.style.padding = `${videoPadding}px`

        resolve()
      }
      videoPlayer.load()
    }
    reader.readAsDataURL(file)
  })
}

function playVideo() {
  playPauseButtonIcon.setAttribute('d', 'M18 10a8 8 0 11-16 0 8 8 0 0116 0zM7 8a1 1 0 012 0v4a1 1 0 11-2 0V8zm5-1a1 1 0 00-1 1v4a1 1 0 102 0V8a1 1 0 00-1-1z')
  videoPlayer.play()
}

function pauseVideo() {
  playPauseButtonIcon.setAttribute('d', 'M10 18a8 8 0 100-16 8 8 0 000 16zM9.555 7.168A1 1 0 008 8v4a1 1 0 001.555.832l3-2a1 1 0 000-1.664l-3-2z')
  videoPlayer.pause()
}

{/* <svg xmlns="http://www.w3.org/2000/svg" class="h-5 w-5" viewBox="0 0 20 20" fill="currentColor">
  <path d="M4.555 5.168A1 1 0 003 6v8a1 1 0 001.555.832L10 11.202V14a1 1 0 001.555.832l6-4a1 1 0 000-1.664l-6-4A1 1 0 0010 6v2.798l-5.445-3.63z" />
</svg>
<svg xmlns="http://www.w3.org/2000/svg" class="h-5 w-5" viewBox="0 0 20 20" fill="currentColor">
  <path d="M8.445 14.832A1 1 0 0010 14v-2.798l5.445 3.63A1 1 0 0017 14V6a1 1 0 00-1.555-.832L10 8.798V6a1 1 0 00-1.555-.832l-6 4a1 1 0 000 1.664l6 4z" />
</svg> */}
