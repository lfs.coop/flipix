var actions = {}
var strokes = [], configs = []
var strokeId = 0, configId = 0
var strokeBuffer = null, actionsBuffer = null

function replaceConfigActionIfNeeded(frame, layer) {
  const prevConfigs = configs.filter((c) => c.layer === layer && c.frame < frame)

  // (if current config has already been registered in previous frame, abort rewrite)
  const c = brushColors[layer], s = brushSizes[layer], t = brushTypes[layer]
  if (
    prevConfigs[prevConfigs.length - 1] &&
    prevConfigs[prevConfigs.length - 1].color === c &&
    prevConfigs[prevConfigs.length - 1].size === s &&
    prevConfigs[prevConfigs.length - 1].type === t
  )
    return

  const registeredConfig = configs.find((c) => c.layer === layer && c.frame === frame)
  if (registeredConfig) {
    // (if current config has already been registered for this frame, abort rewrite)
    if (registeredConfig.color === c && registeredConfig.size === s && registeredConfig.type === t)
      return
    removeConfig(registeredConfig)
  }
  pushPenAction({ type: 'config', layer: currentLayer })
}

function pushPenAction(action) {
  const frame = 'frame' in action ? action.frame : currentVideoFrame
  const isConfig = action.type === 'config'
  if (!isConfig) {
    if (actionsBuffer === null)
      actionsBuffer = {}
    if (!(frame in actionsBuffer))
      actionsBuffer[frame] = []
  }

  const { width: w } = timelineBackground.getBoundingClientRect()
  const r = w / (timelineEndFrame - timelineStartFrame)
  const x = r * (frame - timelineStartFrame)

  // store stroke / config objects
  let id, domId, domElement
  if (isConfig) {
    id = configId
    action.configId = configId
    domId = `config-${id}`
  } else {
    id = strokeId
    action.strokeId = strokeId
    domId = `stroke-${id}`
  }
  if (action.type === 'down') {
    domElement = $el({
      id: domId,
      className: 'action-stroke absolute',
      style: { left: `${x}px`, bottom: `${2 + action.layer * 10}px`, width: '1px' },
      onclick: (e) => {
        e.stopPropagation()
        selectStroke(id)
      }
    })
    strokeBuffer = {
      id,
      domElement,
      start: frame,
      moves: [],
      layer: action.layer,
      initialPosition: { x: action.event.pageX, y: action.event.pageY },
      initialPressure: action.event.pressure,
    }
    strokes.push(strokeBuffer)
  }
  else if (action.type === 'move') {
    if (strokeBuffer === null) return
    strokeBuffer.moves.push({
      x: action.event.pageX,
      y: action.event.pageY,
      pressure: action.event.pressure,
    })
  }
  else {
    const { width: w } = timelineBackground.getBoundingClientRect()
    const r = w / (timelineEndFrame - timelineStartFrame)
    const x = r * (frame - timelineStartFrame)

    const domId = `config-${configId}`
    const removedLayersBefore = removedLayers.filter((l) => l < action.layer)
    domElement = $el({
      id: domId,
      className: 'action-config absolute',
      style: { left: `${x}px`, bottom: `${2 + (action.layer - removedLayersBefore.length) * 10}px` },
      onclick: (e) => {
        e.stopPropagation()
        selectConfig(id)
      }
    })
    action.brushColor = action && action.brushColor ? action.brushColor : brushColors[action.layer]
    action.brushSize = action && action.brushSize ? action.brushSize : brushSizes[action.layer]
    action.brushType = action && action.brushType ? action.brushType : brushTypes[action.layer]
  }

  action.domElement = domElement
  action.id = id
  
  if (isConfig) {
    const oldConfig = configs.find(c => c.frame === frame && c.layer === action.layer)
    if (oldConfig) removeConfig(oldConfig)
    appendContent(timelineBackground, domElement)
    configs.push({
      id,
      domElement,
      frame,
      layer: action.layer,
      color: action.brushColor,
      size: action.brushSize,
      type: action.brushType,
    })
    configId += 1
    if (!(frame in actions))
      actions[frame] = []
    actions[frame].push(action)
  } else {
    actionsBuffer[frame].push(action)
    if (
      action.type === 'up' &&
      actionsBuffer !== null &&
      strokeBuffer !== null
    ) {
      strokeBuffer.end = frame
      mergeActionsBuffer(actionsBuffer, strokeBuffer, r)
      actionsBuffer = null
      strokeBuffer = null
      strokeId += 1
    }
  }

  // enable export and clear
  if (video !== null) {
    btnExport.enable()
    btnClearPenActions.enable()
  }
}

function mergeActionsBuffer(actionsBuffer, strokeBuffer, r) {
  const overwrittenStrokeIds = []
  for (const [frame, acts] of Object.entries(actionsBuffer)) {
    const refStrokeId = (frame in actions) ? actions[frame][0].strokeId : null
    const curStrokeId = acts[0].strokeId
    if (refStrokeId !== null && refStrokeId !== curStrokeId) {
      overwrittenStrokeIds.push(refStrokeId)
    }
    if (!(frame in actions))
      actions[frame] = []
    for (const act of acts) {
      actions[frame].push(act)
      if (act.type !== 'config') {
        const x1 = strokeBuffer.start * r
        const x2 = strokeBuffer.end * r
        strokeBuffer.domElement.style.left = `${x1}px`
        strokeBuffer.domElement.style.width = `${x2 - x1}px`
        appendContent(timelineBackground, strokeBuffer.domElement)
      }
    }
  }

  for (const strokeId of overwrittenStrokeIds) {
    const s = strokes.find(s => s.id === strokeId)
    if (s && s.layer === strokeBuffer.layer)
      removeStroke(s)
  }
}

function pullPenActions(frame) {
  if (!(frame in actions))
    return []
  return actions[frame]
}

function removeStroke(stroke) {
  // remove associated actions
  const newActions = {}
  for (const [frame, acts] of Object.entries(actions)) {
    const filteredActs = acts.filter((action) => action.strokeId !== stroke.id)
    if (filteredActs.length > 0) newActions[frame] = filteredActs
  }
  actions = newActions
  // remove associated dom element
  stroke.domElement.remove()
  // remove data
  strokes.splice(strokes.indexOf(stroke), 1)

  selectedStroke = null
}

function removeConfig(config) {
  // remove associated actions
  const newActions = {}
  for (const [frame, acts] of Object.entries(actions)) {
    const filteredActs = acts.filter((action) => action.configId !== config.id)
    if (filteredActs.length > 0) newActions[frame] = filteredActs
  }
  actions = newActions
  // remove associated dom element
  config.domElement.remove()
  // remove data
  configs.splice(configs.indexOf(config), 1)

  selectedConfig = null
}

async function clearPenActions() {
  // remove all dom elements associated with actions
  for (const stroke of strokes)
    stroke.domElement.remove()
  for (const config of configs)
    config.domElement.remove()

  actions = {}
  strokeBuffer = null
  strokeId = 0
  configId = 0
  strokes = []
  configs = []

  selectedStroke = null
  selectedConfig = null

  // destroy all layers
  for (let i = 0; i < canvases.length; i++) {
    canvases[i].remove()
  }
  canvases = []
  addCanvasHandlers()
  removeChildren(layerPickers)
  createLayerPicker = undefined
  currentLayer = null
  appendContent(layerPickers, $el({
    className: 'layer-picker',
    onclick: async () => await setCurrentLayer(0)
  }))
  await addLayer()
}

function clearPenActionsOnLayer(layer) {
  // remove associated actions
  const newActions = {}
  for (const [frame, acts] of Object.entries(actions)) {
    const filteredActs = acts.filter((action) => action.layer !== layer)
    if (filteredActs.length > 0) newActions[frame] = filteredActs
  }
  actions = newActions

  // remove associated strokes
  const newStrokes = []
  for (const stroke of strokes) {
    if (stroke.layer !== layer) {
      if (stroke.layer > layer) {
        const removedLayersBefore = removedLayers.filter((l) => l < stroke.layer)
        stroke.domElement.style.bottom = `${2 + (stroke.layer - removedLayersBefore.length) * 10}px`
      }
      newStrokes.push(stroke)
    } else {
      stroke.domElement.remove()
    }
  }
  strokes = newStrokes

  // remove associated configs
  const newConfigs = []
  for (const config of configs) {
    if (config.layer !== layer) {
      if (config.layer > layer) {
        const removedLayersBefore = removedLayers.filter((l) => l < config.layer)
        config.domElement.style.bottom = `${2 + (config.layer - removedLayersBefore.length) * 10}px`
      }
      newConfigs.push(config)
    } else {
      config.domElement.remove()
    }
  }
  configs = newConfigs

  selectedStroke = null
  selectedConfig = null
}
