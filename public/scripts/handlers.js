function addCanvasHandlers(canvas = null) {
  const checkedCanvases = canvas === null ? canvases : [canvas]
  for (const canvas of checkedCanvases) {
    canvas.onpointerdown = (e) => {
      if (video === null && !testing) return
      downHandler(e, currentLayer)
    }
    canvas.onpointermove = (e) => {
      moveHandler(e, currentLayer)
    }
    canvas.onpointerup = upHandler
    canvas.onpointerleave = leaveHandler
    canvas.style.pointerEvents = 'all'
  }
}

function toggleShortcuts(on) {
  const onkeydown = (e) => {
    if (e.ctrlKey || e.metaKey) {
      if (e.key === 'r' && video !== null) {
        setRecording(!recording)
      }

      else if (e.key === 't') {
        setTesting(!testing)
      }

      else if (e.key === 'f') {
        setBrushFade(!brushFade, true)
      }

      else if (e.key === 'e' && video !== null) {
        toggleExportModal(true)
      }

      else if (e.key === 'o' && video !== null) {
        loadSession()
      }

      else if (e.key === 's' && video !== null) {
        saveSession()
      }
    }
  }
  if (on)
    window.addEventListener('keydown', onkeydown)
  else
    window.removeEventListener('keydown', onkeydown)
}

function toggleVideoHandlers(on) {
  const onkeydown = (e) => {
    if (document.activeElement && document.activeElement.tagName === 'INPUT')
      return
    if (e.key === 'ArrowLeft') {
      setCurrentVideoFrame(currentVideoFrame - 1)
    } else if (e.key === 'ArrowRight') {
      setCurrentVideoFrame(currentVideoFrame + 1)
    } else if (e.key === 'Space') {
      setPlaying(!playing)
    } else if (e.key === 'Backspace' || e.key === 'Delete' || e.key === 'x') {
      if (selectedStroke !== null) removeStroke(selectedStroke)
      if (selectedConfig !== null) removeConfig(selectedConfig)
    }
  }

  if (on) {
    window.addEventListener('keydown', onkeydown)
  } else {
    window.removeEventListener('keydown', onkeydown)
  }
}

function dragHandle(handle, ondrag) {
  let r, startX = 0, xOffset = 0
  handle.onmousedown = dragMouseDown

  function dragMouseDown(e) {
    const { width: w } = timelineBackground.getBoundingClientRect()
    r = w / totalFramesCount
    e = e || window.event
    e.preventDefault()
    startX = e.clientX
    xOffset = 0
    document.onmouseup = closeDragElement
    document.onmousemove = elementDrag
  }

  function elementDrag(e) {
    e = e || window.event
    e.preventDefault()
    xOffset = e.clientX - startX
    const delta = (Math.abs(xOffset) > 10) ? (xOffset < 0 ? -1 : 1) : 0
    const frame = timelineStartFrame + delta * 10
    if (frame < 0 || frame >= totalFramesCount) return
    ondrag(frame)
    if (delta !== 0)
      startX = e.clientX
  }

  function closeDragElement() {
    document.onmouseup = null
    document.onmousemove = null
  }
}
