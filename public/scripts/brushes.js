function fetchBrushes(callback) {
  const xhr = new XMLHttpRequest()
  url = 'assets/brushes.json'
  xhr.open('GET', url, true)
  xhr.onload = function () {
    if (this.status === 200) {
      callback(JSON.parse(this.responseText))
    }
  }
  xhr.send()
}
