const VERSION = '1.1.5'
var
  brushes,
  brushData = {},
  video = null,

  lastLayer = 1,
  removedLayers = [],
  layerToRemove = null,

  // darkThemeIsOn,

  // brush settings
  initialBrushColor = '#000000',
  initialBrushSize = 15,
  initialBrushType = 'b0',

  brushFade = true,
  brushFadingSpeed = 3,
  brushColors = { 0: initialBrushColor },
  brushSizes = { 0: initialBrushSize },
  brushSizeRatios = { 0: 10 },
  brushTypes = { 0: initialBrushType },
  brushUrls = { 0: null },
  brushConfigs = { 0: null },

  canvasMode = null,

  openAccordionGeneral = true,
  openAccordionConfig = true,

  // video settings
  videoPadding = 40,
  playbackSpeed = 1,
  currentVideoFrame = -1,
  totalFramesCount = -1,
  playing = false,

  timelineZoom = 1,
  timelineStartFrame = 0,
  timelineEndFrame = -1,
  autoplayOnRecord = true,
  videoMuted = false,
  videoLoop = true,

  recording = false,
  testing = false,
  exporting = false,

  exportedFrames = {},
  exportedCanvases = [],
  exportStartFrame = 0,
  exportEndFrame = -1,
  currentExportStartFrame = 0,
  currentExportEndFrame = -1,
  exportMode = 'all',
  exportBeautify = false,
  exportCropMargins = true,
  exportLeft = -1,
  exportTop = -1,
  exportWidth = -1,
  exportHeight = -1,
  exportVideoWidth = -1,
  exportVideoHeight = -1,
  exportVideoScale = 1,

  currentLayer = 0,
  selectedStroke = null,
  selectedConfig = null,

  // util DOM references
  // . main area
  mainArea,
  canvases,
  hiddenCanvas,
  videoContainer,
  videoPlayer,
  recordingOverlay,
  testingOverlay,
  testingCanvasControls,
  exportingOverlay,
  exportMessage,
  timeline,
  playPauseButtonIcon,
  muteButton,
  loopButton,
  layerRemoveHandles,
  layerPickers,
  createLayerPicker,
  timelineStartFrameInput,
  timelineEndFrameInput,
  timelineZoomIndicator,
  timelineZoomIndicatorLeftHandle,
  timelineZoomIndicatorRightHandle,
  timelineZoomIndicatorCurrentFrame,
  timelineZoomIndicatorDragger,
  timelineBackground,
  timelineTotalFramesCountLabel,
  timelineCurrentFrameInput,
  timelineCurrentFrame,
  timelineCurrentLayer,
  timelineReferenceFramesWrapper,
  // . toolbar
  videoPlayer,
  brushFadeCheckbox,
  brushFadingSpeedSlider,
  brushColorInput,
  brushColorPicker,
  colorSwatchesGrid,
  brushSizeSlider,
  sizeRandomSlider,
  angleRandomSlider,
  brushPresetsSelect,
  brushNewPresetNamePanel,
  brushNewPresetNameInput,
  brushTypeButtons,
  btnRecording,
  btnTesting,
  btnExport,
  btnLoadSession,
  btnSaveSession,
  btnClearPenActions,
  // . modals
  removeLayerConfirm,
  changeVideoConfirm,
  removePresetConfirm,
  clearPenActionsConfirm,
  exportModal,
  exportInputZoomedStart,
  exportInputZoomedEnd,
  exportInputCustomStart,
  exportInputCustomEnd,

  // . misc
  currentVideoFileInput,
  toastsHolder

function initialise() {
  // // On page load or when changing themes, best to add inline in `head` to avoid FOUC
  // if (localStorage.theme === 'dark' || (!('theme' in localStorage) && window.matchMedia('(prefers-color-scheme: dark)').matches)) {
  //   darkThemeIsOn = true
  // } else {
  //   darkThemeIsOn = false
  // }

  presets = loadPresets()
  
  fetchBrushes(async (res) => {
    brushes = res

    for (const brush of brushes) {
      brushData[brush.key] = brush
      await new Promise((resolve) => {
        brushData[brush.key].imgData = new Image()
        brushData[brush.key].imgData.src = `assets/brushes_white/${brush.image}`
        brushData[brush.key].imgData.onload = () => resolve()
      })
    }
    
    await createHTML()

    const colorWheel = new iro.ColorPicker('#brush-color-picker', {
      layout: [
        { 
          component: iro.ui.Wheel,
          options: {
            wheelLightness: true,
            wheelAngle: 0,
            wheelDirection: 'anticlockwise',
          } 
        },
        {
          component: iro.ui.Slider,
          options: {
            sliderType: 'value',
            activeIndex: 2
          }
        }
      ],
      width: 120,
      layoutDirection: 'horizontal',
    })
    colorWheel.on('color:change', (color) => setBrushColor(color.hexString))
    colorWheel.el.style.width = 'fit-content'
    colorWheel.el.style.margin = '0 auto'
    colorWheel.color.hexString = initialBrushColor
    brushColorPicker = colorWheel

    // (force init)
    setCurrentLayer(0).then(() => {
      setBrushSize(initialBrushSize)
      setBrushFade(brushFade)
      setBrushFadingSpeed(brushFadingSpeed)
      brushTypeButtons[0].click()
      // setDarkTheme(darkThemeIsOn)
  
      setRecording(false)
      setTesting(false)
      setExporting(false)
      setVideo(null)
      toggleNewPresetNamePanel(false)
      toggleRemoveLayerConfirm(false)
      toggleChangeVideoConfirm(false)
      toggleRemovePresetConfirm(false)
      toggleClearPenActionsConfirm(false)
      toggleExportModal(false)
      // timeline.style.display = 'flex'
      addCanvasHandlers()
      toggleShortcuts(true)

      dragHandle(
        timelineZoomIndicatorDragger,
        (f) => {
          const d = timelineEndFrame - timelineStartFrame
          if (f + d >= totalFramesCount) return
          timelineZoomIndicatorLeftHandle.value = f
          timelineZoomIndicatorRightHandle.value = f + d
          updateTimelineZoomIndicatorLeftHandle()
          updateTimelineZoomIndicatorRightHandle()
        }
      )
    })
  })

  window.onresize = (e) => {
    let width, height
    if (canvasMode === 'video') {
      const bbox = videoPlayer.getBoundingClientRect()
      width = bbox.width
      height = bbox.height
    } else {
      width = e.target.innerWidth - Math.max(window.innerWidth * 0.25, 150)
      height = e.target.innerHeight - 80
    }
    for (const canvas of canvases) {
      canvas.width = width
      canvas.height = height
    }
  }

  window.onbeforeunload = () => {
    toggleShortcuts(false)
  }
}
