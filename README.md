# Flipix

**Flipix** is an **"open video painter"**: it is an open-source tool that allows you to paint on videos in real-time!

It's accessible over here:

## https://lfs.coop.gitlab.io/flipix/

## Features

### Global overview

To use Flipix, you'll want to load up a **video file** (either _.mp4_ or _.mov_); then, you'll be able to **overlay one or more painting layers** on top of it to draw on the video while it's running. When you are finished **recording**, you can **replay** the stored actions or **export** them frame by frame.

When you work on your Flipix **session** you can be in 3 modes:

1. "play mode" (default): you can replay recorded actions (with all the layers overlayed on top of each other). You can also draw on the painting canvas over your video but those actions will not be stored (it's just for testing!).
2. "record mode": you can add new actions (see below for more info) to your session while the video is playing
3. "test mode": you can draw on a blank canvas to test the different available brushes and get used to the tool

The application's interface is organized in **3 main areas**:

1. the video and paint canvas in the top-left corner
2. the timeline at the bottom
3. the toolbar on the right

### Actions

"**Actions**" can be of 2 types: a stroke or a configuration.

1. strokes are recorded whenever you start dragging your mouse on the canvas and then move it around, until your release the left mouse button
2. configuration actions are recorded whenever you change a setting on your brush in the toolbar (its size, its type, its color...)

*Note: if you use a graphics tablet, the pressure of your stylus will be taken into account when you draw!*

To add a new action to your session, you need to enter **recording mode** and then either draw a line in the canvas zone or change a setting. The action will automatically be added for the current video frame.

### Timeline

The timeline gives you an overall intuition of what your current Flipix session contains: it shows you all the currently recorded actions, the total number of frames in the loaded video, the current frame...

![screenshot_timeline](./public/img/screenshot_timeline.png)

*Note: in the timeline, strokes are shown with long bars and configurations are shown as little diamonds.*

It also has some additional buttons to switch the video file for another, play or pause the video, fast-forward, rewind and mute/unmute.

The timeline is divided in 2 horizontal parts: the current view zone at the bottom and its position within the complete video at the top. This feature is inspired by video editing software like After Effects where you have:

- a "zoomed out" vision of your session that always encompasses the entire length of the video
- a dynamic "zoomed in" vision that can give you a more detailed view of your frames.

To move through the timeline, you can either click directly to jump to a frame, use the frame inputs or use the arrow keys to move one frame left or right.

### Toolbar

The toolbar is divided in 3 subparts.

#### General settings

Flipix lets you control several things in your session:

- the margins around the video (to add some padding areas where you can start your stroked but that won't be exported)
- the playback speed of the video (to slow down the video speed and make more precise drawings)
- whether strokes should fade over time, and if so at which speed (the higher the speed, the faster the strokes disappear)

![screenshot_general-settings](./public/img/screenshot_general-settings.png)

#### Brush settings

Brushes have 3 options - the color, the size and the style.

- you can create and reload presets to quickly get a pre-defined set of options for your brush
- below, the color wheel allows you to pick the color for your brush, and the little "+" button at the bottom lets you create swatches to create a palette of colors easy to re-use
- then, you can set the size of the brush: by default, the scale goes from 15 to 120 (arbitrary units) but you can modify it by hand if you need smaller or larger values
- finally, the "style" is the stamp that will be pasted all along your mouse path when you draw - it's what determines the "texture" of your stroke (pencil, felt, paint brush...)

![screenshot_brush-settings](./public/img/screenshot_brush-settings.png)

<u><b>Color picker</b></u>

The color wheel allows you to pick any color you want: the disk gives you a tint (hue) and saturation and the band on the right gives the luminosity of the color.

You can also enter directly a color hexadecimal code in the input above the picker to set the brush to this color. Conversely, once you've picked a value with the wheel, you can easily copy the hexadecimal value of the current color with the "copy" button.

<u><b>Brush presets</b></u>

To browse and choose a preset from a previous session, click on the dropdown and pick an item from the list.

![screenshot_preset-choose](./public/img/screenshot_preset-choose.png)

To create a new preset from your current configuration (color, size, style), click on the "+" button on the right of the dropdown and enter a name for your preset. Then press `<Enter>` to add the preset to the list. It will be selected by default.

![screenshot_preset-create](./public/img/screenshot_preset-create.png)

You can update a preset that you created previously: simply select it in the dropdown, update the values and click the "arrows" button on the right of the dropdown to set this new configuration as the one stored in the preset.

You can also delete a preset you created previously by clicking on the "-" button on the right of the dropdow. Simply select the preset you want to remove, then press the button: you'll get a confirm modal to make sure that you really want to delete the preset.

**⚠️ Warning:** Brush presets are stored locally in the browser, so Flipix will only reload the ones that you saved on this computer and _this browser_.

#### Interaction buttons

Those buttons allow you to perform various core actions:

- toggle the recording mode on or off
- toggle the test mode on or off
- frame export
- load/save the session (to a JSON file)
- clear all recorded actions

![screenshot_interaction-buttons](./public/img/screenshot_interaction-buttons.png)

Note that the two first sections can be collapsed to reduce the amount of info shown on the screen.

### Layers

Flipix offers a system of **layers** to **decouple your actions** and record multiple strokes at the same frame. For example, here, I have two layers on top of each other and some **actions overlap** in the middle:

![screenshot_layers](./public/img/screenshot_layers.png)

When you record actions they are added to the current **active layer**.

By default, you have 1 layer. You can then **create more layers** on the left of the timeline by clicking the "+" sign. To switch between layers and change which one is **active**, click on the circle on the left of the layer line. You can also remove a layer by clicking the "x" cross left of the layer line.

**⚠️ Warning:** if you create a new action on a layer that already contains some for these frames, the previous actions will be erased and replaced by your new stroke/configuration actions.

### Export

For now, the frame export gathers all the actions your recorded in your session and replays them to rebuild the drawing on transparent PNG files, without the video in the background. This allows you to **composite** the drawings in some video editing software and have a better control of the exported strokes.

You'll get a downloadable zip archive that contains one folder per non-empty layer; then, inside each folder, you'll get one image per action pack.

_**Important note:** because browser events are quite fast, you actually have more than one action recorded for each frame of the movie. So, when you export your session to PNG files, you might have more images than there are frames in your movie. Those intermediary frames should then be recompressed linearly in some video editing soft to reconstruct the sequence of actions without losing the details of the mouse path._

When you click on the export button, you'll get a modal with a few parameters to set:

![screenshot_export](./public/img/screenshot_export.png)

So, in this modal, you can choose:

- if the padding should be removed or kept as-is
- whether the output images should be scaled up or down. For now, the size of the images depends on the size of your drawing canvas in the browser, meaning that it can be smaller than the original video. To restore the original size, you might need to scale up at export time.
- finally, you can choose what time span you want to export. By default, the entire session is exported; but you can also export just the current "zoomed in" part of the timeline, or even a custom time range.

### Shortcuts

There are a few shortcuts to Flipix to perform some actions more quickly:

| Shortcut | Action |
|----------|--------|
| `<Ctrl+R>` / `<Cmd+R>` | Toggle the record mode on/off |
| `<Ctrl+T>` / `<Cmd+T>` | Toggle the test mode on/off |
| `<Ctrl+F>` / `<Cmd+F>` | Toggle strokes fading on/off |
| `<Ctrl+E>` / `<Cmd+E>` | Show the export modal |
| `<Ctrl+O>` / `<Cmd+O>` | Open a Flipix session file (.JSON) |
| `<Ctrl+O>` / `<Cmd+O>` | Save the Flipix session (to a .JSON file) |

## Contributing

Any new ideas of features or bug fixes are welcome! Feel free to open issues on this repository and/or create pull requests if you would like to participate in the development of Flipix :)

Below is an overview of the repo architecture and a quick description of each file.

```
.
└─ assets/: images/brush icons/stamps to load
    └─ ...
└─ img/: screenshots & images for the doc
    └─ ...
└─ scripts/: the core JS engine of Flipix
    ├─ actions.js: management of actions recording
    ├─ brushes.js: utils for brush data (fetching)
    ├─ callbacks.js: callback functions for DOM elements
    ├─ canvas.js: management of the drawing canvases
    ├─ components.js: definition of handy shortcuts to create specific DOM elements
    ├─ dom.js: description/creation of the dynamic DOM tree
    ├─ handlers.js: event handlers for specific user actions (keyboard events, dragging...)
    ├─ main.js: initialization of the page on window load
    ├─ math.js: util math functions
    ├─ presets.js: management of the brush presets
    ├─ session.js: management of session load/save
    ├─ swatches.js: management of color swatches
    ├─ tools.js: misc util functions
    └─ video.js: util functions for the video player
└─ styles/: CSS files for styling
    ├─ components.css: custom styling for the gallery of components
    └─ main.css: styling for the DOM elements of the page
└─ vendors/: third-party libraries
    └─ ...
├─ index.html: main entry-point for the browser
└─ README.md: repository main doc file
```
